<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
?>
<?php $manager->loadBodyhead(); ?>

<section id="team" class="team">
      <div class="container fadeInUp" data-aos="">

        <div class="section-title">
          <h3>Über Uns</h3>
          <p>Mitarbeiter, die wissen, was sie tun. Höchste Professionalität, Qualität und Engagement bringt dieses Quartett an jedem Arbeitstag. Die Motivation? Folgendes Zitat:
            <br>
            <br>
            <h4>"Wir wissen nicht, was wir nicht wissen." ~ Ulexander Ahrich, 2021 </h4></p>
        </div>

        <div class="row">
          <div class="col d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="/assets/img/team/team-1.jpg" class="img-fluid" alt="">
                <div class="social social-links">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Shurichovski Uhrichov</h4>
                <span>Chief Executive Officer</span>
              </div>
            </div>
          </div>

          <div class="col d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="/assets/img/team/team-2.jpg" class="img-fluid" alt="">
                <div class="social social-links">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Alaxanda Uhra</h4>
                <span>Accountant</span>
              </div>
            </div>
          </div>

          <div class="col d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="/assets/img/team/team-3.jpg" class="img-fluid" alt="">
                <div class="social social-links">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Ulexander Ahrich</h4>
                <span>Product Manager</span>
              </div>
            </div>
          </div>

        <div class="col d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="/assets/img/team/team-4.jpg" class="img-fluid" alt="">
                <div class="social social-links">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Schoko Uhrich</h4>
                <span>CTO</span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>