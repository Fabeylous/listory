<?php 
session_start();
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
$manager->validateLoggedIn(false);
$id = $_SESSION['user'];
$data = $manager->getUser($id);
extract($data);
?>
<?php $manager->loadBodyhead(); ?>

<link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css">
<div class="container bg-white mt-5 mb-5">

	  <div class="tab-content">

      <div class="nav nav-tabs mb-5" id="nav-tab" role="tablist">
        
        <button class="nav-link active" id="profilesettings-tab" data-bs-toggle="tab" data-bs-target="#profilesettings" type="button" role="tab" aria-controls="profilesettings" aria-selected="true">Profileinstellungen-Einstellungen</button>
        
        <button class="nav-link" id="orderhistory-tab" data-bs-toggle="tab" data-bs-target="#orderhistory" type="button" role="tab" aria-controls="orderhistory" aria-selected="false">Bestellhistorie</button> 

      </div>

    <div class="tab-content" id="nav-tabContent">

      <div class="tab-pane fade active show" id="profilesettings" role="tabpanel" aria-labelledby="profilesettings-tab">
		    <div class="row">
		        <div class="col-md-3 border-right" style="border-right: 1px solid gray;">
		            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
		            	<img class="rounded-circle mt-5" width="150px" src="https://avatars.githubusercontent.com/u/98681?v=4">
		            	<span class="font-weight-bold"><?= $username; ?></span>
		            	<span class="text-black-50"><?= $mail; ?></span>

		            	<div class="list-group">
									  <button type="button" name="enableEditProfile" class="btn list-group-item list-group-item-action" aria-current="true">
									    Profil bearbeiten
									  </button>
									  <a href="/app/logout.php" class="btn list-group-item list-group-item-action" aria-current="true">
									    Ausloggen
									  </a>
									  <button type="button" class="btn list-group-item list-group-item-action" aria-current="true">
									    Konto löschen
									  </button>
									</div>
		            </div>
		        </div>
		        <div class="col-md-9 border-right">
		            <div class="p-3 py-5">
		                <div class="d-flex justify-content-between align-items-center mb-3">
		                    <h4 class="text-right">Profildaten</h4>
		                </div>
		                <div class="row mt-2">
		                    <div class="col-md-6">
		                    	<label class="labels">Username</label>
		                    	<input type="text" class="form-control" id="username" value="<?= $username ?>" disabled>
		                    </div>
		                    <div class="col-md-6">
		                    	<label class="labels">E-Mail</label>
		                    	<input type="text" class="form-control"   id="mail" value="<?= $mail ?>" disabled>
		                    </div>
		                </div>
		                <div class="row mt-2">
		                    <div class="col-md-6">
		                    	<label class="labels">Name</label>
		                    	<input type="text" class="form-control" placeholder="Vorname" id="firstName" value="<?= $firstname ?>" disabled>
		                    </div>
		                    <div class="col-md-6">
		                    	<label class="labels">Nachname</label>
		                    	<input type="text" class="form-control" placeholder="Nachname"  id="lastName" value="<?= $lastname ?>" disabled>
		                    </div>
		                </div>
		                <div class="row mt-3">
		                    <div class="col-6">
		                    	<label class="labels">Straße</label>
		                    	<input type="text" class="form-control" placeholder="Straße"  id="street" value="<?= $street ?>" disabled>
		                    </div>
		                    <div class="col-2">
		                    	<label class="labels">PLZ</label>
		                    	<input type="text" class="form-control" placeholder="PLZ" id="zip"  value="<?= $zip ?>" disabled>
		                    </div>
		                    <div class="col-4">
		                    	<label class="labels">Stadt</label>
		                    	<input type="text" class="form-control" placeholder="Stadt"  id="city" value="<?= $city ?>" disabled>
		                    </div>
		                </div>

		            </div>

		            <div class="p-3 py-5">
		                <div class="d-flex justify-content-between align-items-center mb-3">
		                    <h4 class="text-right">Passwort ändern</h4>
		                </div>
		                <div class="row mt-2">
		                    <div class="col-md-4">
		                    	<label class="labels">Aktuelles Passwort</label>
		                    	<input type="password"  id="currentPassword"  class="form-control" placeholder="******"  disabled>
		                    </div>
		                    <div class="col-md-4">
		                    	<label class="labels">Neues Passwort</label>
		                    	<input type="password"  id="newPassword" class="form-control"  placeholder="Neues Passwort" disabled>
		                    </div>
		                    <div class="col-md-4">
		                    	<label class="labels">Neues Passwort wiederholen</label>
		                    	<input type="password" id="passwordConfirmation" class="form-control"  placeholder="Neues Passwort wiederholen" disabled>
		                    </div>
		                </div>

		            </div>

		            <div class="p-3" style="display:none;" id="saveProfileButton">
		            	<button class="btn btn-primary btn-lg btn-block" name="saveProfileButton">Speichern</button>
		            </div>
		        </div>
		    </div>
      </div>


      <div class="tab-pane fade" id="orderhistory" role="tabpanel" aria-labelledby="orderhistory-tab">
		    	<table id="orderstable" class="table table-striped table-hover mt-5">
		    		<thead>
		    			<tr>
		    				<th>Bestelldatum</th>
		    				<th>Bestellnummmer</th>
		    				<th>Produkte</th>
		    				<th>Gesamtpreis</th>
		    				<th>Detailansicht</th>
		    			</tr>
		    		</thead>
		    		<tbody>
				    	<? 
				    	foreach ($manager->getUserOrders($id) as $key => $value) {
				    		$prod = unserialize($value['products']);
				    		?>
				    		<tr>
				    			<td><?= date('d.m.Y, H:i',strtotime($value['timestamp'])); ?></td>
				    			<td>#<?= sprintf('%08d',$value['ID']);?></td>
				    			<td>
				    				<?php 
				    				foreach ($prod as $productid => $amount) {
				    					$product = $manager->getProduct(false,$productid)[0];
				    					echo $amount.'x '.$product['title'].'<br>';
				    				}
				    				?>
				    			</td>
				    			<td><?= number_format($value['price'],2,',','.'); ?>€</td>
				    			<td>
				    				<a class="btn btn-primary" href="/profile/vieworder/<?= $value['ID']; ?>/">Rechnung ansehen</a>
				    			</td>
				    		</tr>
				    		<?php 
				    	}
				    	?>		    			
		    		</tbody>
		    	</table>

					<?= $manager->loadScript('datatable.js'); ?>

		    	<script type="text/javascript">
						jQuery('#orderstable').dataTable( {
						  "language": {
						    "url": "/assets/local/de_de.json"
						  }
						} );
					</script>
      </div>

    </div>


  </div>


</div>