<?php 
session_start();
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
$manager->validateLoggedIn(false);
?>
<?php $manager->loadBodyhead(); ?>

<div class="loginmask">
<div class="container py-5 h-100">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="row text-center p-3">
                	<div class="col-2 p-4">
                		<img src="/media/logo.jpg" class="img-fluid">
                	</div> 
                	<div class="col-10">
                		<h1 class="p-5" style="float:right"> Rechnung <span class="badge rounded-pill bg-primary">#<?= sprintf('%08d',$order['ID']);?> </span></h1>
                	</div>
                </div>
                <hr>
                <div class="table-responsive p-2">
                    <table class="table table-borderless">
                        <tbody>
                            <tr class="add">
                                <td>Lieferadresse</td>
                                <td>Rechnungsadresse</td>
                            </tr>
                            <tr class="content">
                                <td class="font-weight-bold"><?= $order['shipping_street']; ?> <br><?= $order['shipping_city']; ?></td>
                                <td class="font-weight-bold"><?= $order['invoice_street']; ?> <br><?= $order['invoice_city']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="products p-2">
                    <table class="table table-borderless">
                        <tbody>
                            <tr class="add">
                                <td>Buchtitel</td>
                                <td>Menge</td>
                                <td>Einzelpreis</td>
                                <td class="text-center">Gesamt</td>
                            </tr>
                            <?php 
                            foreach (unserialize($order['products']) as $productid => $amount) {
                            	$pr = $manager->getProduct(false,$productid)[0];
                            	?>
	                            <tr class="content">
	                                <td><a href="/product/<?= $pr['slug']; ?>/" target="_blank"><?= $pr['title']; ?></a><br><small><?= $pr['authorname']; ?></small></td>
	                                <td><?= $amount; ?></td>
	                                <td><?= number_format($pr['price'],2,',','.'); ?>€</td>
	                                <td class="text-center"><strong><?= number_format($pr['price'] * $amount,2,',','.'); ?>€</strong></td>
	                            </tr>
                            	<?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="products p-2">
                    <table class="table table-borderless">
                        <tbody>
                            <tr class="add">
                                <td class="text-center">
                                    <h3>Gesamtpreis</h3>
                                </td>
                            </tr>
                            <tr class="content">
                                <td class="text-center">
                                    <h2><?= number_format($order['price'],2,',','.'); ?>€</h2>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
            </div>
        </div>
	</div>
  </div>
</div>

<style type="text/css">
	

.add td {
    color: #c5c4c4;
    text-transform: uppercase;
    font-size: 12px
}

.content {
    font-size: 14px
}
</style>