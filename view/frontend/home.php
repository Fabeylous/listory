<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

?>
    <?php $manager->loadBodyhead($headhome); ?>
    <section id="hero" class="d-flex align-items-center">
      <div class="container fadeInUp">
      
        <div class="position-relative overflow-hidden p-3  text-center">
          <div class="col-md-5 p-lg-5 mx-auto my-5 intro-text">
            <h1 class="display-4 font-weight-normal">Willkommen auf listory</h1>
            <hr>
            <p class="lead font-weight-normal">Jedes Buch hat seine <strong>Story</strong>.<br>Für die passende Bücher<strong>list</strong>e sorgen wir.<br>
              Egal ob Bücherankauf, -verkauf oder nur Leihen - auf <strong>listory</strong> wird jeder fündig.</p>
            <a class="btn btn-outline-secondary" href="#suggestions">Buchempfehlungen von uns</a>
          </div>
        </div>

      </div>
    </section>


    <section id="featured-services" class="featured-services">
        <div class="container">

          <div class="row">
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
              <div class="icon-box">
                <div class="icon"><i class="bi bi-currency-euro"></i></div>
                <h4 class="title"><a href="">Buch verkaufen</a></h4>
                <p class="description">Auf <strong>listory</strong> kannst du deine Bücher weiterverkaufen. Wir bieten einen Ankauf-Service an, damit auch jedes Buch das passende Regal findet.</p>
              </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
              <div class="icon-box">
                <div class="icon"><i class="bi bi-arrow-left-right"></i></div>
                <h4 class="title"><a href="">Buch leihen</a></h4>
                <p class="description">Du willst nur ein Buch ausleihen? Du kannst dich hier direkt mit dem Verkäufer in Kontakt setzen und gegebenenfalls für eine kleine Gebühr das Buch ausleihen!</p>
              </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
              <div class="icon-box">
                <div class="icon"><i class="bi bi-bag"></i></div>
                <h4 class="title"><a href="">Buch kaufen</a></h4>
                <p class="description">Das passende Buch gefunden? Perfekt! Kaufe es direkt hier, ob neu oder gebraucht, originalverpackt oder mit vintage-spuren.</p>
              </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
              <div class="icon-box">
                <div class="icon"><i class="bi bi-envelope-open"></i></div>
                <h4 class="title"><a href="">Kontakt aufnehmen</a></h4>
                <p class="description">Falls ein Buch auf <strong>listory</strong> nicht im Sortiment sein sollte, kann jederzeit ein Exemplar angefragt werden! Wir bemühen uns, alle Wünsche zu berücksichtigen.</p>
              </div> 
            </div>

          </div>

        </div>
    </section>

    <section id="suggestions" class="section-bg">
      <div class="text-center container">

        <div class="section-title">
          <h3><span>Buchempfehlungen</span></h3>
          <p>Falls du dir unschlüssig bist, welches Buch du kaufen willst: Probiere es doch mit einem unserer Empfehlungen. Diese Bücher sind persönlich von uns gewählt und haben uns komplett überzeugt!</p>
        </div>

        <div class="bg-dark shadow-sm mx-auto" style="width: 80%; border-radius: 21px 21px 0 0;">
          <div id="productcarousel" class="carousel slide p-3" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <?php 
                foreach($manager->getProducts(false, 6,true) as $k => $product) {
                  ?>

                  <button type="button" data-bs-target="#productcarousel" data-bs-slide-to="<?= $k; ?>"<?= ($k == 1 ? 'class="active"':'') ?> aria-current="true" aria-label="<?= $product['title']; ?>"></button>
                  <?php
                }
                ?>
            </div>
            <div class="carousel-inner">


            <div class="row row-cols-1 row-cols-md-2 g-4">
              <?php 
              foreach($manager->getProducts(false, 6,true) as $k => $product) {
                ?>
                <div class="carousel-item <?= ($k == 1?'active':'')?> ">
                  <div class="carousel-background"></div>
                  <a href="/product/<?= $product['slug'] ?>" class="text-decoration-none text-white">
                    <img src="/media/catalog/products/<?= $product['ID'] ?>/cover.jpg" class="carousel-image rounded mx-auto d-block" alt="<?= $product['title'] ?>">
                  </a>
                  <? /*
                  <div class="container">
                      <div class="carousel-caption">
                        <a href="/product/<?= $product['slug'] ?>" class="text-decoration-none text-white">

                          <h1><?= $product['title'] ?></h1>
                            <span>Ein <?= $product['genrename']?>-Buch von <?= $product['authorname'] ?></span> 
                            <hr>
                            <p><?= $product['subtitle'] ?></p>      
                          <!-- <p><a class="btn btn-lg btn-info" href="#">Zum Buch</a></p> -->
                        </a>
                      </div>
                  </div>
                  */

                  ?>
                </div>

                <?php
              } ?>
            </div>

            

            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#productcarousel" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Zurück</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#productcarousel" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Vor</span>
            </button>
          </div>
        </div>
      </div>
    </section>


    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">
          <h3>Frequently Asked <span>Questions</span></h3>
          <p>Eine Auflistung der häufig gestellten Fragen (FAQ) kannst du hier finden.</p>
        </div>

        <div class="row justify-content-center">
          <div class="col-xl-10">
            <ul class="faq-list">
              <?php foreach ($manager->getFAQs() as $faq) {
                ?>

                  <li>
                    <div data-bs-toggle="collapse" class="collapsed question" href="#faq-<?= $faq['ID'] ?>">
                      <?= $faq['question'] ?>
                      <i class="bi bi-chevron-down icon-show"></i>
                      <i class="bi bi-chevron-up icon-close"></i>
                    </div>
                    <div id="faq-<?= $faq['ID'] ?>" class="collapse" data-bs-parent=".faq-list">
                      <p>
                        <?= $faq['answer']; ?>
                      </p>
                    </div>
                  </li>

                <?php
              }
              ?>


            </ul>
          </div>
        </div>

      </div>
    </section>

    <section id="clients" class="clients section-bg">
      <div class="container aos-init aos-animate" data-aos="zoom-in">

        <div class="row">

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-2.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-3.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-4.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-5.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-6.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section>

    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h3><span>Kontakt</span></h3>
          <p>Weitere Fragen zu einem Produkt? Du findest dein Lieblingsbuch nicht? Du möchtest ein Buch verkaufen? Kontaktiere uns gerne!</p>
        </div>   
        <?php $siteinfo = $manager->getOptions('siteinfo'); ?>

        <div class="row">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bi bi-map"></i>
              <h3>Unseres Adresse</h3>
              <p><?= $siteinfo[1]['value'] ?>, <?= $siteinfo[2]['value'] ?></p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bi bi-envelope"></i>
              <h3>E-Mail schreiben</h3>
              <p><?= $siteinfo[3]['value'] ?></p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bi bi-telephone"></i>
              <h3>Anrufen</h3>
              <p><?= $siteinfo[4]['value'] ?></p>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-lg-6 ">
            <iframe class="mb-4 mb-lg-0" src="https://maps.google.com/maps?q=Heilbronn&t=&z=13&ie=UTF8&iwloc=&output=embed" style="border:0; width: 100%; height: 384px;" allowfullscreen="" frameborder="0"></iframe>
          </div>

          <div class="col-lg-6">
            <form action="#" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Name" required="">
                </div>
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="E-Mail Adresse" required="">
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Betreff" required="">
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" placeholder="Nachricht" required=""></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Lade...</div>
                <div class="error-message"></div>
                <div class="sent-message">Die Nachricht wurde versendet. Vielen Dank! Wir melden uns so bald wie möglich.</div>
              </div>
              <div class="text-center"><button type="submit">Senden</button></div>
            </form>
          </div>

        </div>
      </div>
    </section>