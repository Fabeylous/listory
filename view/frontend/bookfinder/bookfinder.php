<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$products = $manager->getProducts();

$genres = $manager->getGenres();
$authors = $manager->getAuthors();

?>
<?php $manager->loadBodyhead(); ?>
<div class="container mb-4">
	<div class="row d-flex justify-content-center mt-5 ">


    <aside class="col-3 sidebar"> 
          <div class="authors sidebar-section p-2">
            <button name="resetFilter" class="btn btn-outline-secondary btn-block">Filter zurücksetzen</button>
          </div>

          <div class="search sidebar-section p-2">
              <div class="heading d-flex justify-content-between align-items-center">
                  <span class="text-uppercase"><strong>Textsuche</strong></span>
              </div>

              <div id="searchfield" class="inputs"> 
                <input id="searchfield-input" type="text" class="form-control" placeholder="ISBN- oder Titelsuche"> 
              </div>
          </div>
          <div class="authors sidebar-section p-2">
              <div class="heading d-flex justify-content-between align-items-center">
                  <span class="text-uppercase"><strong>Authoren</strong></span>
              </div>
              <div class="selection">
                  <div class="alphfilter">
                    <span class="filterer" data-only="abcd">ABCD</span>
                    <span class="filterer" data-only="efgh">EFGH</span>
                    <span class="filterer" data-only="ijkl">IJKL</span>
                    <span class="filterer" data-only="mnop">MNOP</span>
                    <span class="filterer" data-only="qrst">QRST</span>
                    <span class="filterer" data-only="uvw">UVW</span>
                    <span class="filterer" data-only="xyz">XYZ</span>
                    <span class="filterer" data-only="all">Alle</span>
                  </div>
                <div class="p-lists">
                  <?php foreach ($authors as $author) : ?> 
                    <div class="d-flex justify-content-between mt-2  clickable-filter" data-filter="author-<?= $author['ID'] ?>"> 
                      <span class="filterme"><?= $author['author']; ?></span> 
                      <span><?= $author['bookscount']; ?></span> 
                    </div>
                  <?php endforeach; ?>

                    <div class="d-flex justify-content-between mt-2" style="display:none !important"> 
                      <span class="dontfilterme filterer" data-only="all">Versteckte Autoren [...]</span> 
                      <span></span> 
                    </div>

                </div>
              </div>
          </div>        

          <div class="authors sidebar-section p-2">
                <div class="heading d-flex justify-content-between align-items-center">
                    <span class="text-uppercase"><strong>Genres</strong></span>
                </div>
                <div class="selection">
                  
                  <div class="alphfilter">
                    <span class="filterer" data-only="abcd">ABCD</span>
                    <span class="filterer" data-only="efgh">EFGH</span>
                    <span class="filterer" data-only="ijkl">IJKL</span>
                    <span class="filterer" data-only="mnop">MNOP</span>
                    <span class="filterer" data-only="qrst">QRST</span>
                    <span class="filterer" data-only="uvw">UVW</span>
                    <span class="filterer" data-only="xyz">XYZ</span>
                    <span class="filterer" data-only="all">Alle</span>
                  </div>
                  <div class="p-lists">
                    <?php foreach ($genres as $genre) : ?> 
                      <div class="d-flex justify-content-between mt-2 clickable-filter" data-filter="genre-<?= $genre['ID'] ?>"> 
                        <span class="filterme"><?= $genre['genre']; ?></span> 
                        <span><?= $genre['books']; ?></span> 
                      </div>
                    <?php endforeach; ?>

                      <div class="d-flex justify-content-between mt-2" style="display:none !important"> 
                        <span class="dontfilterme filterer" data-only="all">Versteckte Genres [...]</span> 
                        <span></span> 
                      </div>

                  </div>
                </div>
          </div>

      </aside>

      <div class="col-8">
          <div class="card">
            <div class="filterloader" style="display:none;"></div>
              <div class="row g-1" id="resultfield">
                <?php foreach ($products as $product) : ?>
                    <?php $manager->generateProductcard($product); ?>
                <?php endforeach; ?> 
              </div>
          </div>
     </div>



      <style type="text/css">
      .active-filter {
        font-weight: bold;
        color: var(--bs-green);
      }

      .filterloader {
        height: 100%;
        width: 100%;
        position: fixed;
        background: #ffffffa6;
        z-index: 2;
        top: 0;
        left: 0;
      }
      h6 {
        width: 75%;
        height: 50px;
      }
      .clickable-filter {
        cursor: pointer;
      }
      .clickable-filter:hover {
        color: var(--bs-secondary);
      }
      .img-container {
          position: relative
      }

      .img-container .first {
          position: absolute;
          width: 100%;
      }

      .img-container img {
          border-top-left-radius: 5px;
          border-top-right-radius: 5px;
      }

      .product-detail-container {
          padding: 10px;
      }

      .ratings i {
          color: #a9a6a6
      }

      .ratings span {
          color: #a9a6a6
      }
      .sidebar-section {

    margin-top: 5px;
    border-bottom: 1px solid #eee

      }
      </style>


  </div>
</div>
<?php $manager->loadScript('bookfinder.js'); ?> 
