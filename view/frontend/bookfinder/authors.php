<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
?>
<?php $manager->loadBodyhead(); ?>


<section id="genres" class="d-flex align-items-center">
    <div class="container fadeInUp">
        <h1 class="text-center">Autoren</h1>
        <hr>
        
    <?php 
    $count = 0;
    $authorarr = $manager->getAuthors();

    foreach ($authorarr as $key => $author) { 
      if($count % 3 == 0) {
        if($count != 0) echo '</div>';
        ?>
        <div class="row g-2 m-2 p-2">
        <?php
      }
      ?>
        <div class="col-md-4">
            <div class="card p-3 text-center px-4">
                <div class="user-image"> <img src="/media/authors/<?=$author['ID'];?>/profile.jpg" class="rounded-circle" width="80"> </div>
                <div class="user-content">
                    <h5 class="mb-0"><strong><?= $author['author']; ?></strong></h5> <span><?= $author['nationality']; ?></span>
                    <p><?= $author['biography']; ?></p>
                </div>
                <div class="authorinfo row"> 
                  <div class="col-12">
                    <?php 
                      if($author['bookscount'] == 1) {
                        $button = '<a href="/bookfinder?q=author-'.$author['ID'].'" class="btn btn-primary btn-block">Buch entdecken</a>';
                      } elseif($author['bookscount'] == 0) {
                        $button = '<span>Dieser Autor hat leider keine Bücher mehr auf <strong>listory</strong>.</span>';
                      } else {
                        $button = '<a href="/bookfinder?q=author-'.$author['ID'].'" class="btn btn-primary btn-block">'.$author['bookscount'] . ' Bücher entdecken</a>';
                      }

                      echo $button;
                    ?>
                    
                  </div>
                </div>
            </div>
        </div>
        <?php
      if(!isset($authorarr[$key+1])) {
        ?>
        </div>
        <?php
      }
      $count++;
      ?>
    <?php } ?>

    </div>
</section>
<style type="text/css">
  .card{
    height: 100%;
  }
  .user-content{
    height: 100%;
  }
  .rounded-circle {
    object-fit: cover;
    width:100px;
    height:100px;
  }

</style>