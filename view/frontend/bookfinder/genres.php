<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
?>
<?php $manager->loadBodyhead(); ?>


<section id="genres" class="d-flex align-items-center">
    <div class="container fadeInUp">
        <h1 class="text-center">Folgende Genres bieten wir an</h1>
        <hr>
        <div class="row">
        
    <?php foreach ($manager->getGenres() as $genre) { ?>
                <div class="service-block-two col-lg-3 col-md-6 col-sm-12">
                    <a href="/bookfinder/?q=genre-<?= $genre['ID']; ?>" class="text-decoration-none stretched-link ">
                        <div class="inner-box">
                            <div class="shape-one"></div>
                            <div class="shape-two"></div>
                            <h5><?= $genre['genre']; ?></h5>
                            <div class="text"><? //echo $genre['description']; ?></div>
                            </a>
                        </div>
                    </a>
                </div>
    <?php } ?>

        </div>
    </div>
</section>