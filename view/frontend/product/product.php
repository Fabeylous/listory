<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
?>
<div class="container main-container">
    <?php 
        $manager->loadBodyhead(); 
        $product = $product[0];


        $directory  = BASE_DIR. '/media/catalog/products/'.$product['ID'];
        $images = glob($directory . "/*.jpg");
        $imgArr = array();
        foreach($images as $image)
        {
            array_push($imgArr, '/media/catalog/products/'.$product['ID'].'/'.basename($image));
        }


    ?>
<div class="container mt-5 mb-5 fadeInUp">
    <div class="card">
        <div class="row g-0">
            <div class="col-md-6 border-end">
                <div class="d-flex flex-column justify-content-center" style="height: 100%;">
                    <div class="productbackground"></div>
                    <div class="main_image"> <img src="<?= $imgArr[0]; ?>" id="main_product_image"> </div>
                    <div class="thumbnail_images">
                        <ul id="thumbnail">
                            <?php
                            foreach ($imgArr as $img) {
                                ?>
                             <li><img onclick="changeImage(this)" src="<?= $img; ?>" width="70"></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="p-3 right-side fullheight">
                    <div class="third">
                        <div class="d-flex justify-content-between align-items-center">
                            <h2><?= $product['title'] ?></h2>
                        </div>
                        <div class="mt-2 pr-3 content">
                            <p class="lead product-sub"><?= $product['subtitle'] ?></p>
                        </div>
                        <div>
                              <span>Ein <?= $product['genrename']?>-Buch von <strong><?= $product['authorname'] ?></strong></span> 
                        </div>
                            <hr>
                        <div class="product-desc">
                            <p class="lead"><?= $product['description']; ?></p>
                        </div>
                            <hr>
                        <h3 class="productprice"><?= number_format($product['price'],2,',','.'); ?>€</h3> 
                        <hr>
                        <?php 
                        $mon    =  array('Mär','Mai','Okt','Dez');
                        $en     =  array('Mar','May','Oct','Dec');
                        ?>
                        <div>
                            <table  class="table table-striped table-hover">
                                <tr>
                                    <td>ISBN</td>
                                    <td><?= $product['isbn']; ?></td>
                                </tr>
                                <tr>
                                    <td>Erscheinungsdatum</td>
                                    <td><?= str_replace($en,$mon, date('d. M Y', strtotime($product['published']))); ?></td>
                                </tr>
                                <tr>
                                    <td>Seitenanzahl</td>
                                    <td><?= $product['pages']; ?></td>
                                </tr>
                                <tr>
                                    <td>Sprache</td>
                                    <td><?= $product['lang']; ?></td>
                                </tr>
                            </table>
                        </div>                       
                    </div>
                    <div class="row align-items-end twothird"> 
                        <div class="col">                            
                            <button class="btn btn-block btn-product btn-outline-primary">Leihe Anfragen</button> 
                        </div>
                        <div class="col">
                            <button class="btn btn-block btn-product btn-primary" name="addProductToCart" data-productid="<?= $product['ID']; ?>">In den Warenkorb legen</button>                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
      <div class="col">
        <div class="card card-cover product-card h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg">
          <div class="d-flex flex-column h-100 p-5 text-white text-shadow-1">
            <a class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold stretched-link text-decoration-none text-white" href="/bookfinder/">Zur Buchsuche</a>
            <span>Damit auch jedes Buch ein passendes Regal findet.</span>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card card-cover product-card h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg">
          <div class="d-flex flex-column h-100 p-5 text-white text-shadow-1">
            <a class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold stretched-link text-decoration-none text-white" href="/bookfinder/?q=author-<?= $product['author']; ?>"><?= $product['authorname']; ?></a>
            <span>Finden Sie weitere atemberaubende Bücher von <strong><?= $product['authorname']?></strong></span>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card card-cover product-card h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg">
          <div class="d-flex flex-column h-100 p-5 text-white text-shadow-1">
            <a class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold stretched-link text-decoration-none text-white" href="/bookfinder/?q=genre-<?= $product['genre']; ?>"><?= $product['genrename'];?>-Bücher</a>
            <span>Passend zu diesem Genre finden Sie hier mehr <strong><?= $product['genrename']; ?>-Bücher</strong>, die perfekt zu diesem passen.</span>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
    function changeImage(element) {
        var main_prodcut_image = document.getElementById('main_product_image');
        main_prodcut_image.src = element.src;
    }
</script>

<style type="text/css">
body {
    background-color: #ecedee
}
</style>