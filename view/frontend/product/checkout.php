  <?php 
session_start();

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$manager->loadBodyhead();
//$_SESSION['cart']['total'] total
//$_SESSION['cart']['products'] produkte, wobei $_SESSION['cart']['products'] ein Array enthält mit ID => MENGE
$totalPrice = 0;
if (isset($_GET['success'])) {
  echo '<div class="mt-5 mb-5 text-center">
          <h2 id="checkout-success">Danke für die Bestellung!</h2>
          <span>Du wirst in kürze <strong>keine</strong> E-Mail erhalten.<br>Deine Bestellung wird bestimmt irgendwann bearbeitet. Danke für Dein Vertrauen in <strong>listory.</strong></span><br><br><br>
          <span>Wie wäre es mit noch mehr Büchern? Unser Bestand ist endlos, da sowieso keine Bücher versandt werden.</span>
        </div>';
  $manager->checkout($_POST);
} else {
if(!$manager->validateLoggedIn(false)) { $user = 0; } else { $user = $_SESSION['user']; }

if($manager->validateLoggedIn(false)) {
      $user = $manager->getUser($_SESSION['user']);
} 
  ?>

<div class="signupmask">
<div class="container mb-5 py-5 h-100">

    <div class="row signupmaskcontainer fadeInDown">
      <div class="col-md-5 col-lg-4 order-md-last">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-primary">Warenkorb</span>
          <span class="badge bg-primary rounded-pill"><?= $_SESSION['cart']['total']; ?></span>
        </h4>
        <ul class="list-group mb-3">
          <?php foreach ($_SESSION['cart']['products'] as $product => $amount) { 
  			$product = $manager->getProduct(false,$product)[0]; 
  			$totalPrice = $totalPrice + ($product['price'] * $amount);
        $_SESSION['cart']['totalPrice'] = $totalPrice;
          ?>
          <li class="list-group-item d-flex justify-content-between lh-sm">
            <div>
              <h6 class="my-0"><?= $amount ?>x - <?= $product['title'] ?> </h6>
              <small class="text-muted"><?= $product['subtitle'] ?> </small>
            </div>
            <span class="text-muted"><strong><?= number_format($product['price'],2,',','.'); ?>€</strong></span>
          </li>
          <?php } ?>
          <!-- <li class="list-group-item d-flex justify-content-between bg-light">
            <div class="text-success">
              <h6 class="my-0">Promo code</h6>
              <small>EXAMPLECODE</small>
            </div>
            <span class="text-success">−$5</span>
          </li>
		      -->
          <li class="list-group-item d-flex justify-content-between">
            <h6>Gesamtpreis</h6>
            <span><strong><?= number_format($totalPrice,2,',','.'); ?>€</strong></span>
          </li>
        </ul>

        <form class="card p-2">
          <div class="input-group">
            <a href="/cart/" class="btn btn-secondary btn-block">Zurück zum Warenkorb</a>
          </div>
        </form>
      </div>
      <div class="col-md-7 col-lg-8">
        <form class="needs-validation" method="POST" action="?success">
          <div class="shippingaddress">
              <div class="divider d-flex align-items-center my-4">
                <p class="text-center fw-bold mx-3 mb-0 text-muted">LIEFERADRESSE</p>
              </div>
            <div class="row g-3">
              <div class="col-sm-6">
                <label for="firstName" class="form-label">Vorname</label>
                <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="<?= $user['firstname']; ?>" required>
              </div>

              <div class="col-sm-6">
                <label for="lastName" class="form-label">Nachname</label>
                <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="<?= $user['lastname']; ?>" required>
              </div>

              <div class="col-12">
                <label for="email" class="form-label">E-Mail</label>
                <input type="email" class="form-control" name="email" id="email" value="<?= $user['mail']; ?>">
              </div>

              <div class="col-12">
                <label for="address" class="form-label">Adresse</label>
                <input type="text" class="form-control" name="address" id="address" placeholder="Buchstraße 1" value="<?= $user['street']; ?>" required>
              </div>

              <div class="col-12">
                <label for="address2" class="form-label">Adresse 2 <span class="text-muted">(Optional)</span></label>
                <input type="text" class="form-control" name="address2" id="address2" placeholder="Zusatz zur Adresse">
              </div>

              <div class="col-md-6">
                <label for="zip" class="form-label">Postleitzahl</label>
                <input type="text" placeholder="Buchstadt"  <?= (!empty($user['zip']) ? 'value="'.$user['zip'].'"' : ''); ?> class="form-control" name="zip" id="zip" required>
              </div>

              <div class="col-md-6">
                <label for="zip" class="form-label">Stadt</label>
                <input type="text" class="form-control" name="city" id="city" placeholder="Buchstadt" <?= (!empty($user['city']) ? 'value="'.$user['city'].'"' : ''); ?> required>
              </div>
            </div>

          </div>

          <div class="form-check mt-2">
            <input type="checkbox" class="form-check-input" name="same-addres" id="same-address" checked >
            <label class="form-check-label" for="same-address">Die Lieferadresse ist auch die Rechnungsadresse.</label>
          </div>

          <div class="invoiceaddress" name="invoiceaddress-containe" id="invoiceaddress-container" style="display:none;">
            <div class="divider d-flex align-items-center my-4">
              <p class="text-center fw-bold mx-3 mb-0 text-muted">RECHNUNGSADRESSE (Fake :D )</p>
            </div>
            <div class="row g-3">
              <div class="col-sm-6">
                <label for="fakeFirst" class="form-label">Vorname</label>
                <input type="text" class="form-control" name="fakeFirst" id="fakeFirst" placeholder="" value="<?= $user['firstname']; ?>">
              </div>

              <div class="col-sm-6">
                <label for="fakeSecond" class="form-label">Nachname</label>
                <input type="text" class="form-control" name="fakeSecond" id="fakeSecond" placeholder="" value="<?= $user['lastname']; ?>">
              </div>

              <div class="col-12">
                <label for="fakeMail" class="form-label">E-Mail</label>
                <input type="email" class="form-control" name="fakeMail" id="fakeMail" value="<?= $user['mail']; ?>">
              </div>

              <div class="col-12">
                <label for="notWorking" class="form-label">Adresse</label>
                <input type="text" class="form-control" name="notWorking" id="notWorking" placeholder="Buchstraße 1" value="<?= $user['street']; ?>">
              </div>

              <div class="col-12">
                <label for="stillNotWorking" class="form-label">Adresse 2 <span class="text-muted">(Optional)</span></label>
                <input type="text" class="form-control" name="stillNotWorking" id="stillNotWorking" placeholder="Zusatz zur Adresse">
              </div>

              <div class="col-md-6">
                <label for="notHappening" class="form-label">PLZ</label>
                <input type="text" placeholder="Buchland"  <?= (!empty($user['zip']) ? 'value="'.$user['zip'].'"' : ''); ?> class="form-control" name="notHappening" id="notHappening">
              </div>

              <div class="col-md-6">
                <label for="notAtAll" class="form-label">Stadt</label>
                <input type="text" class="form-control" name="notAtAll" id="notAtAll" placeholder="Buchstadt" <?= (!empty($user['city']) ? 'value="'.$user['city'].'"' : ''); ?>>
              </div>
            </div>

          </div>


          <?php if(!$manager->validateLoggedIn(false)) { ?>

          <hr class="my-4">
          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="save-info">
            <label class="form-check-label" for="save-info">Account erstellen nach Abschluss der Bestellung</label><br>
            <span class="text-muted">Durch das Erstellen eines Accounts spart man sich das mühsame Ausfüllen im Checkout. Außerdem bekommt jeder Neukunde ein Gutschein über 5,00€!</span>
          </div>

          <hr class="my-4">
          <?php } ?>
              <div class="divider d-flex align-items-center my-4">
                <p class="text-center fw-bold mx-3 mb-0 text-muted">ZAHLUNGSART</p>
              </div>

          <div class="gy-3">
            <div class="form-check" >
              <input id="btc-radio" name="paymentMethod" type="radio" class="form-check-input" checked="">
              <label class="form-check-label" for="btc-radio">Bitcoin</label><br>
            </div>
              <div id="btc-content"> 
               <img src="/media/bitcoin/Your_Bitcoin_QR_Code.png" width="200"><br>Bitte an die angegebene Bitcoin Adresse senden und die TXID im nächsten Feld angegeben.</img><br>
               <input placeholder="TXID" id="txid" required></input>
              </div>

            <div class="form-check" id="credit-radio">
              <input id="credit-card" name="paymentMethod" type="radio" class="form-check-input">
              <label class="form-check-label" for="credit-card">Kreditkarte</label>
            </div>
              <div id="credit-card-information" class="row">
                <div class="col-4">
                  <label for="cc-name" class="form-label">Name auf der Kreditkarte</label>
                  <input type="text" class="form-control" id="cc-name" placeholder="" >
                  <small class="text-muted">angegebener Name auf der Kreditkarte</small>
                </div>

                <div class="col-4">
                  <label for="cc-number" class="form-label">Kreditkartennummer</label>
                  <input type="text" class="form-control" id="cc-number" placeholder="" >
                </div>

                <div class="col-2">
                  <label for="cc-expiration" class="form-label">Ablaufdatum</label>
                  <input type="text" class="form-control" id="cc-expiration" placeholder="" >
                </div>

                <div class="col-2">
                  <label for="cc-cvv" class="form-label">Sicherheitscode</label>
                  <input type="text" class="form-control" id="cc-cvv" placeholder="" >
                </div>
              </div>


            <div class="form-check" >
              <input id="firstBorn" name="paymentMethod" type="radio" class="form-check-input">
              <label class="form-check-label" for="firstBorn">Erstgeborenes</label><br>
            </div>

            <div class="form-check" >
              <input id="notAtAllLol" name="paymentMethod" type="radio" class="form-check-input">
              <label class="form-check-label" for="notAtAllLol">Gar nicht</label><br>
            </div>

          </div>


          <hr class="my-4">
          <input type="hidden" name="checkaction" id="checkaction" value="checkout">
          <button class="w-100 btn btn-primary btn-lg" type="submit">Bestellung abschließen</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php $manager->loadScript('checkout.js'); 