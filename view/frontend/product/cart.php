<?php 
session_start();

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$manager->loadBodyhead();
?>


<section class="h-100" id="cart" style="background-color: #eee;">
  <div class="container h-100 py-5" style="background-color: white;">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-10">
        <?php if($_SESSION['cart']['total'] == 0) {
          ?>
          <p class="text-center">
            Warenkorb ist leer. Und das ist traurig. <br>
            Suche doch <a href="/bookfinder/"><strong>hier</strong></a> nach einem Buch oder lasse dich von <a href="<?= $manager->baseurl; ?>#suggestions"><strong>unseren Empfehlungen</strong></a> inspirieren!
          </p>
          <?php
        } else {
            foreach($_SESSION['cart']['products'] as $prodid => $amount): ?>
            <?php 
              $product = $manager->getProduct(false,$prodid)[0]; 
            ?>
            <div class="cartcard card rounded-3">
              <div class="card-body p-4">
                <div class="row d-flex justify-content-between align-items-center">
                  <div class="col-md-2 col-lg-2 col-xl-2">
                    <a href="<?= '/product/'.$product['slug'] ?>" class="text-decoration-none">
                      <img src="/media/catalog/products/<?= $product['ID'] ?>/cover.jpg" class="img-fluid rounded-3" alt="<?= $product['title']; ?>">
                    </a>
                  </div>

                  <div class="col-md-3 col-lg-3 col-xl-3">
                    <a href="<?= '/product/'.$product['slug'] ?>" class="text-decoration-none text-dark">
                      <p class="lead fw-bold mb-2">
                        <?= $product['title']; ?>
                      </p>
                      <p>
                        <a href="/bookfinder?q=author-<?= $product['author']; ?>" class="text-decoration-none text-muted">
                          <?= $product['authorname']; ?>
                        </a>
                      </p>
                    </a>
                  </div>

                  <div class="col-md-3 col-lg-3 col-xl-2 d-flex">
                    <button class="cartbtn px-2" name="decreaseAmount" data-productid="<?= $prodid ?>"onclick="this.parentNode.querySelector('input[type=number]').stepDown()">
                      <i class="bi bi-dash-square" style="font-size: 22px;"></i>
                    </button>

                    <input min="0" name="quantity-<?= $prodid ?>" value="<?= $amount; ?>" type="number" class="form-control form-control-sm" style="text-align: center;">

                    <button class="cartbtn px-2" name="increaseAmount" data-productid="<?= $prodid ?>" onclick="this.parentNode.querySelector('input[type=number]').stepUp()">
                      <i class="bi bi-plus-square" style="font-size: 22px;"></i>
                    </button>
                  </div>

                  <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                    <h5 class="mb-0"><?= number_format($product['price'],2,',','.'); ?>€</h5>
                  </div>

                  <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                    <button class="text-primary cartbtn" data-element="quantity-<?= $prodid ?>" data-productid="<?= $prodid ?>" name="removeProductFromCart"><i class="bi bi-trash-fill" style="font-size: 25px;"></i></button>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>

          <div class="divider d-flex align-items-center">
            <p class="text-center fw-bold mx-3 mb-0 text-muted">RABATTCODE</p>
          </div>
          <div class="card mb-4">
            <div class="card-body p-4 d-flex flex-row">
              <div class="form-outline flex-fill">
                <input type="text" id="form1" class="form-control" />
              </div>
              <button type="button" class="btn btn-primary ms-3">Anwenden</button>
            </div>
          </div>

          <div class="divider d-flex align-items-center my-4">
            <p class="text-center fw-bold mx-3 mb-0 text-muted"></p>
          </div>


          <div class="card">
            <div class="card-body">
              <a class="btn btn-outline-primary btn-block btn-lg HOVER mb-2" href="/checkout/" role="button" style="background-color: white;">
                <span></span>
                <text>
                      Zum Checkout
                </text>
              </a>
            </div>
          </div>

        <?php } ?>

      </div>
    </div>
  </div>
</section>


<style type="text/css">
  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }
</style>
