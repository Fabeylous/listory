<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

?>
<?php $manager->loadBodyhead(); ?>
<div class="container ">
	<div class="row d-flex justify-content-center mt-5 ">
        <div class="col-md-8">
            <div class="card">
                <div id="searchfield" class="inputs"> <i class="fa fa-search"></i> <input id="searchfield-input" type="text" class="form-control " placeholder="Suchen Sie nach ISBN, Name, Beschreibung, Titel"> </div>
              <!-- Search results--> 
                  <div class="mt-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center"> <span class="star"><i class="fa fa-star yellow"></i></span>
							<table id="results-table" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                    <th>ISBN</th>
                                    <th>Buchtitel</th>
                                    <th>Autor</th>
                                    <th>Preis</th>
                                  </tr>
                            	</thead>
                            	<tbody id="">
									<tr class="results-tr">
                            			<td class="results-td"></td>
                        			</tr>
                       			</tbody>
                      		</table>
                    	</div>
                	</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>
<?php $manager->loadScript('bookfinder.js'); ?> 