<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
?>
<?php $manager->loadBodyhead(); ?>

<div class="loginmask">
<div class="container py-5 h-100">
  <div class="row d-flex align-items-center justify-content-center h-100">


    <div class="col-2"></div>
    <div class="col-8 loginmaskcontainer fadeInDown">
      <div class="align-items-center justify-content-center d-flex mb-2">
        <h2>Einloggen</h2>
      </div>

      <form action="<?= BASE_URL; ?>/app/validate.php" method="post">
        <div class="form-outline mb-4">
          <input type="text" id="login" name="login" class="form-control form-control-lg" />
          <label class="form-label" for="login">Username</label>
        </div>
        <div class="form-outline mb-4">
          <input type="password" id="password" name="password" class="form-control form-control-lg" />
          <label class="form-label" for="password">Passwort</label>
        </div>

        <div class="d-flex justify-content-around align-items-center mb-4">
          
          <a href="/signup/" class="text-decoration-underline">Jetzt Registrieren!</a>
          <a href="#!" class="text-decoration-underline">Passwort vergessen?</a>
        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Anmelden</button>
	   </form>
    </div>

	   <div class="col-2"></div>
    </div>
  </div>
</div>
