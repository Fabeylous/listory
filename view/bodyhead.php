<?php 
session_start();

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

if(!$headhome) {
  $urladdition = $manager->baseurl;
} else {
  $urladdition = '';
}


$request = $_SERVER['REQUEST_URI'];
?>

<nav class="d-flex flex-wrap align-items-center justify-content-center fadeInDown justify-content-md-between py-3 mb-4 border-bottom fixed-top navbar navbar-expand-sm navbar-light" id="navbar">
              
      <div class="col-2"></div>
      <?php
      if($headhome) {
      ?> 
      <a href="/" class="text-decoration-none navbar-brand hideonmobile" style="position: fixed;">
            <img src="/media/logo.jpg" alt="listory" class="logo">
      </a>  
      <?php 
      } 
      ?>
       <ul class="navbar-nav nav col-8 mb-2 justify-content-center mb-md-0 navbar-collapse collapse" id="navlist">
          <li style="width: 120px;">
            <a href="<?= $urladdition ?>#suggestions" class="nav-link link-dark anchor-link">Empfehlungen</a>
          </li>

          <li style="width: 120px;">
            <a href="<?= $urladdition ?>#faq" class="nav-link link-dark anchor-link">FAQs</a>
          </li>

          <li style="width: 120px;">
            <a href="<?= $urladdition ?>#contact" class="nav-link link-dark anchor-link">Kontakt</a>
          </li>

          <?php if(!$headhome) { ?> 
              <li>
                <a href="/" class="text-decoration-none">
                  <img src="/media/logo_text.jpg" class="logo-small" alt="listory" style="max-height: 50px;">
                </a>
              </li>
          <?php } ?>
          
          <li style="width: 120px;">
            <a href="<?= $urladdition ?>/bookfinder/" class="nav-link link-dark anchor-link <?= (strpos($request, '/bookfinder/') !== false ? 'active':'' ); ?>">Buchfinder</a>
          </li>

          <li style="width: 120px;">
            <a href="<?= $urladdition ?>/authors/" class="nav-link link-dark anchor-link <?= (strpos($request, '/authors/') !== false ? 'active':'' ); ?>">Autoren</a>
          </li>

          <li style="width: 120px;">
            <a href="<?= $urladdition ?>/genres/" class="nav-link link-dark anchor-link <?= (strpos($request, '/genres/') !== false ? 'active':'' ); ?>">Genres</a>
          </li>
        </ul>

        <ul class="profile-nav nav col-2">        
          <?php if(!$manager->validateLoggedIn(false))  {?>
            <li><a class="nav-link link-dark anchor-link loginbtn <?= (strpos($request, '/login/') !== false ? 'active':'' ); ?>" href="/login/">Login</a></li>
            <li><a class="nav-link link-dark anchor-link signupbtn <?= (strpos($request, '/signup/') !== false ? 'active':'' ); ?>" href="/signup/">Registrieren</a></li>
          <?php }else{ ?>
            <li class="dropdown">
              <a href="<?= $urladdition ?>/profile/" class="<?= (strpos($request, '/profile/') !== false ? 'active':'' ); ?>">
                <span>Profil</span> 
                <i class="bi bi-chevron-down"></i>
              </a>
              <ul>
                <li><a href="<?= $urladdition ?>/profile/" class="nav-link link-dark anchor-link">Profil</a></li>
                <li><a href="<?= $urladdition ?>/app/logout.php" class="nav-link link-dark anchor-link">Ausloggen</a></li>
              </ul>
          </li>
          <?php } ?>


            <li>
              <a href="/cart/" class="nav-link link-dark anchor-link anchor-link-special">
                <i class="bi bi-cart" style="font-size:24px"></i>
                <span class="badge badge-primary" id="lblCartCount"><?= (empty($_SESSION['cart']['total'])?'0':$_SESSION['cart']['total']);?> </span>
              </a>
            </li>
          </ul>
          <button class="navbar-toggler collapsed fadeInDown" type="button" data-bs-toggle="collapse" data-bs-target="#navlist" aria-controls="navlist" aria-expanded="false" aria-label="Toggle navigation">
            <img src="/media/logo_text.jpg" style="width: 140px;">
          </button>
    </nav>

    <div id="top"  style="height:11vh;"></div>