<!DOCTYPE html>
<html>
<head>
<title>listory</title>
<meta charset="utf-8">
<meta name="robots" content="noindex, nofollow">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link rel="stylesheet" href="/assets/css/bootstrap.css">
<link rel="stylesheet" href="/assets/css/custom.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="/media/favicon/favicon.ico" type="image/x-icon">
<link rel="icon" type="image/png" href="/media/favicon//favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="/media/favicon//favicon-32x32.png" sizes="32x32">

  <?php 
  //Custom Header Tags (from database?)
  ?>
</head>
<body>
<div id="preloader"></div>