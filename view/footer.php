<?php 

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

?>

<a href="#top" class="back-to-top d-flex align-items-center justify-content-center anchor-link"><i class="bi bi-arrow-up-short"></i></a>
<footer  id="footer" class="footer mt-auto py-3 bg-light">	

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Newsletteranmeldung</h4>
            <p>Melde dich zu unserem Newsletter an, um keine neuen Produkte zu verpassen.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Anmelden">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">
          <?php $siteinfo = $manager->getOptions('siteinfo'); ?>
          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>listory<span>.</span></h3>
            <p>
              <?= $siteinfo[0]['value'] ?><br>
              <?= $siteinfo[1]['value'] ?><br>
              <?= $siteinfo[2]['value'] ?><br>
              <br><br>
              <strong>Telefon:</strong>  <?= $siteinfo[4]['value'] ?><br>
              <strong>E-Mail:</strong> <?= $siteinfo[3]['value'] ?><br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Hilfreiche Links</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a href="/">Home</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/about/">Über uns</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/tac/">AGB</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="/policy/">Datenschutz</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Unsere Sponsoren</h4>
            <ul>
              <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://www.audi.de/de/brand/de.html">Audi</a></li>
              <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://www.redbull.com/de-de/">Red Bull</a></li>
              <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://www.adidas.de/">Adidas</a></li>
              <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://www.volkswagen.de/de.html">Volkswagen</a></li>
              <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://www.tesla.com/de_de">Tesla</a></li>
              <li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://coinmarketcap.com/de/">CoinMarketCap</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Social Media</h4>
            <p>Besuche uns auf Social Media</p>
            <div class="social-links mt-3">
              <a target="_blank" href="https://twitter.com/elonmusk" class="twitter"><i class="bi bi-twitter"></i></a>
              <a target="_blank" href="https://de-de.facebook.com/zuck" class="facebook"><i class="bi bi-facebook"></i></a>
              <a target="_blank" href="https://www.instagram.com/snoopdogg/?hl=de" class="instagram"><i class="bi bi-instagram"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="copyright">
        © <?= date('Y'); ?> Copyright <strong><span>FLAU</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
      </div>
    </div>
  
      <?php 
      $manager->loadScript('bootstrap.bundle.min.js');
      $manager->loadScript('custom.js');
      $manager->loadScript('main.js');
      ?>
  </footer>
</body>
</html>