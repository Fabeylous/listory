<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

$request = explode('?',$_SERVER['REQUEST_URI'])[0];
$r = explode('/', $request);
$authorid = $r[5];
$author = $manager->getAuthors($authorid)[0];


?>
<div class="bg-light" id="newauthor-form" style="width:100%">
  <h2><?= $author['author']; ?></h2><hr>

  <div class="input-fields admin-tab-content ">

    <div class="input-group mb-3">
      <span class="input-group-text" id="authorname-description">Authorname</span>
      <input name="authorname" type="text" value="<?= $author['author']; ?>" class="form-control productinfo-input" placeholder="Name" aria-label="Name" aria-describedby="authorname-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="birthday-description">Geburtstag</span>
      <input name="birthday" type="date" value="<?= $author['birthday']; ?>" class="form-control productinfo-input" placeholder="Name" aria-label="Name" aria-describedby="birthday-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="nationality-description">Geburtsort</span>
      <input name="nationality" type="text" value="<?= $author['nationality']; ?>" class="form-control productinfo-input" placeholder="Geburtsort" aria-label="Geburtsort" aria-describedby="nationality-description">
    </div>

    <input type="hidden" name="authorid" value="<?= $author['ID']; ?>">
    <div class="row">
      <div class="col-3 d-grid">
        <button type="button" data-bs-toggle="modal" data-bs-target="#deleteAuthorModal" class="btn btn-outline-danger">Löschen</button>
      </div>
      <div class="col-6"></div>
      <div class="col-3 d-grid">
        <button type="button" name="editAuthor" class="btn btn-outline-primary">Speichern</button>
      </div>  
    </div> 


  </div>

  </div>
</div>

<div class="modal fade" id="deleteAuthorModal" tabindex="-1" aria-labelledby="deleteAuthorModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteAuthorModalLabel">Sicher?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-footer">
        <button type="button" name="deleteAuthor" class="btn btn-danger">Endgültig löschen</button>
      </div>
    </div>
  </div>
</div>
