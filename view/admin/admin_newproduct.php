<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);


?>
<div class="bg-light" id="admin-products" style="width:100%">
  <h2>Neues Buch anlegen</h2><hr>

  <div class="input-fields admin-tab-content ">

    <div class="input-group mb-3">
      <span class="input-group-text" id="isbn-description">ISBN</span>
      <input name="isbn" type="text"  class="form-control productinfo-input" placeholder="ISBN" aria-label="ISBN" aria-describedby="isbn-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="title-description">Buchtitel</span>
      <input name="title" type="text" class="form-control productinfo-input" placeholder="Buchtitel" aria-label="Buchtitel" aria-describedby="title-description">
    </div>

    <div class="input-group mb-3">
      <label class="input-group-text" for="author">Autor</label>
      <select name="author" class="form-select productinfo-input" id="author">
            <option value="" selected disabled>Wählen...</option>
        <? foreach ($manager->getAuthors() as $auth) {
          ?>
            <option value="<?= $auth['ID']; ?>"><?= $auth['author'] ?></option>
          <?php
        }
        ?>
      </select>
    </div>
<!-- 
    <div class="input-group mb-3">
      <span class="input-group-text" id="slug-description">URL Slug</span>
      <input name="slug" type="text" class="form-control productinfo-input" placeholder="URL Slug (leer lassen für automatische Generierung)" aria-label="URL Slug" aria-describedby="slug-description">
    </div> -->

    <div class="input-group mb-3">
      <span class="input-group-text" id="published-description">Erstveröffentlichung</span>
      <input name="published" type="date" class="form-control productinfo-input" placeholder="Erstveröffentlichung" aria-label="Erstveröffentlichung" aria-describedby="published-description">
    </div> 

    <div class="input-group mb-3">
        <span class="input-group-text" id="subtitle-description">Subtitel</span>
        <input name="subtitle" type="text" class="form-control productinfo-input" placeholder="Subtitel" aria-label="Subtitel" aria-describedby="subtitle-description">
    </div>


    <div class="input-group mb-3">
      <span class="input-group-text">Beschreibung</span>
      <textarea name="description" class="form-control productinfo-input" productinfo-input aria-label="Beschreibung"></textarea>
    </div>

    <div class="input-group mb-3">
      <label class="input-group-text" for="bookgenre">Genre</label>
      <select name="bookgenre" class="form-select productinfo-input" id="bookgenre">
            <option value="" selected disabled>Wählen...</option>
        <? foreach ($manager->getCategories() as $cat) {
          ?>
            <option value="<?= $cat['ID']; ?>"><?= $cat['genre'] ?></option>
          <?php
        }
        ?>
      </select>
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="price-description">Preis</span>
      <input name="price" type="number" step="0.01" class="form-control productinfo-input" placeholder="Preis" aria-label="Preis" aria-describedby="price-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="pages-description">Seitenanzahl</span>
      <input  name="pages" type="number" class="form-control productinfo-input" placeholder="Seitenanzahl" aria-label="Seitenanzahl" aria-describedby="pages-description">
    </div>

    <div class="input-group mb-3">
      <label class="input-group-text" for="booklanguage">Sprache</label>
      <select name="booklanguage" class="form-select productinfo-input" id="booklanguage">
            <option value="" selected disabled>Wählen...</option>
        <? foreach ($manager->getLanguages() as $lang) {
          ?>
            <option value="<?= $lang; ?>"><?= $lang ?></option>
          <?php
        }
        ?>
      </select>
    </div>


    <div class="input-group mb-3">
      <span class="input-group-text" id="amount-description">Menge</span>
      <input name="amount" type="text" class="form-control productinfo-input" placeholder="Menge" aria-label="Menge" aria-describedby="amount-description">
    </div>


    <div class="input-group mb-3">
      <span class="input-group-text" id="intention-description">Intention</span>
      <select name="intention" class="form-select productinfo-input" id="intention">
            <option value="" selected disabled>Wählen...</option>
            <option value="sell">Verkaufen</option>
            <option value="loan">Leihen</option>
            <option value="all">Verkaufen oder leihen</option>
      </select>
    </div>

    <div class="row">
      <div class="col-9"></div>
      <div class="col-3 d-grid">
        <button type="button" name="newProduct" class="btn btn-outline-primary">Anlegen</button>
      </div>
    </div> 


  </div>

  </div>
</div>