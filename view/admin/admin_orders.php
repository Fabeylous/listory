<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

?>
<link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css">
<div class="bg-light" id="admin-orders" style="width:100%">
  <h2>Bestellungen</h2><hr>
  <div class="admin-tab-content">
    <table id="authortable" class="table table-striped table-hover">
      <thead class="table-primary">
              <tr>
                <th>Bestelldatum</th>
                <th>Name</th>
                <th>Benutzer-Daten</th>
                <th>Benutzer-Daten</th>
                <th>Bestellnummmer</th>
                <th>Produkte</th>
                <th>Gesamtpreis</th>
              </tr>
            </thead>
            <tbody>
              <? 
              foreach ($manager->getUserOrders() as $key => $value) {
                $prod = unserialize($value['products']);
                $user = $manager->getUser($value['user']);
                ?>
                <tr>
                  <td><?= date('d.m.Y, H:i',strtotime($value['timestamp'])); ?></td>
                  <td><?= $user['firstname'] . ' ' .$user['lastname'];?></td>
                  <td><?= date('d.m.Y, H:i',strtotime($value['timestamp'])); ?></td>
                  <td>#<?= sprintf('%08d',$value['ID']);?></td>
                  <td>
                    <?php 
                    foreach ($prod as $productid => $amount) {
                      $product = $manager->getProduct(false,$productid)[0];
                      echo $amount.'x '.$product['title'].'<br>';
                    }
                    ?>
                  </td>
                  <td><?= number_format($value['price'],2,',','.'); ?>€</td>
                  <td>
                    <a class="btn btn-primary" href="/profile/vieworder/<?= $value['ID']; ?>/">Details ansehen</a>
                  </td>
                </tr>
                <?php 
              }
              ?>              
            </tbody>
      <tfoot class="table-primary">
        <tr>
          <th colspan="7"></th>
        </tr>
        
      </tfoot>
  </div>
</div>
<?= $manager->loadScript('datatable.js'); ?>

<script type="text/javascript">
  jQuery('#authortable').dataTable( {
      "language": {
        "url": "/assets/local/de_de.json"
      }
    } );
</script>


    
  </div>
</div>