<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

?>
<div class="bg-light" id="newauthor-form" style="width:100%">
  <h2>Neuen Autor hinzufügen</h2><hr>

  <div class="input-fields admin-tab-content ">

    <div class="input-group mb-3">
      <span class="input-group-text" id="authorname-description">Authorname</span>
      <input name="authorname" type="text" class="form-control productinfo-input" placeholder="Name" aria-label="Name" aria-describedby="authorname-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="birthday-description">Geburtstag</span>
      <input name="birthday" type="date" class="form-control productinfo-input" placeholder="Name" aria-label="Name" aria-describedby="birthday-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="nationality-description">Geburtsort</span>
      <input name="nationality" type="select" class="form-control productinfo-input" placeholder="Geburtsort" aria-label="Geburtsort" aria-describedby="nationality-description">
    </div>

    <div class="row">
      <div class="col-9"></div>
      <div class="col-3 d-grid">
        <button type="button" name="addAuthor" class="btn btn-outline-primary">Hinzufügen</button>
      </div>
    </div> 


  </div>

  </div>
</div>