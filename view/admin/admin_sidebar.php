<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

?>

  <div class="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark h-100" style="width: 30vh;min-height: 100vh;">
    <a href="/" class="d-flex justify-content-center">
      <img src="/media/logo.jpg" id="icon" alt="User Icon" />
    </a>
  <hr>
  <ul class="nav nav-pills flex-column mb-auto admin-sidebar">
    <li>
      <a class="nav-link text-white text-left <?= ($type == 'dashboard'? 'active':'')?>" id="admin-dashboard-tab" href="/holygrail/panel/" style="width:100%">
        <i class="bi-speedometer2"></i>
        Dashboard
      </a>
    </li>
    <li>
      <a class="nav-link text-white text-left <?= ($type == 'authors'? 'active':'')?>" id="admin-dashboard-tab" href="/holygrail/panel/authors/" style="width:100%">
        <i class="bi bi-pen"></i>
        Autoren
      </a>
    </li>
    <li>
      <a class="nav-link text-white text-left <?= ($type == 'orders'? 'active':'')?>" id="admin-orders-tab" href="/holygrail/panel/orders/" style="width:100%">
        <i class="bi-table"></i>
        Bestellungen
      </a>
    </li>
    <li>
      <a class="nav-link text-white text-left <?= ($type == 'products'? 'active':'')?>" id="admin-products-tab" href="/holygrail/panel/products/" style="width:100%" >
        <i class="bi-grid"></i>
        Bücherliste
      </a>
    </li>
    <li>
      <a class="nav-link text-white text-left <?= ($type == 'customers'? 'active':'')?>" id="admin-customers-tab" href="/holygrail/panel/customers/" style="width:100%">
        <i class="bi-person-circle"></i>
        Kunden
      </a>
    </li>
    <li>
      <a class="nav-link text-white text-left <?= ($type == 'settings'? 'active':'')?>" id="admin-settings-tab" href="/holygrail/panel/settings/"  style="width:100%">
        <i class="bi-gear-wide-connected"></i>
        Einstellungen
      </a>
    </li>
  </ul>
    <hr>
    <div class="dropdown">
      <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
      <img src="https://github.com/mdo.png" alt="" class="rounded-circle me-2" width="32" height="32">
      <strong><?= $user['firstname'] . ' ' . $user['lastname']; ?></strong>
    </a>
      <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
        <li><a class="dropdown-item" href="/profile/">Settings</a></li>
        <li><a class="dropdown-item" href="/profile/">Profile</a></li>
        <li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="/app/logout.php">Sign out</a></li>
      </ul>
    </div>
  </div>