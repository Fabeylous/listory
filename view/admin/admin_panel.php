<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

if(!$manager->validateLoggedIn()) {
  $manager->loadDeniedAccess();
  exit();
}



$user = $manager->getUser($_SESSION['user']);
?>
<link rel="stylesheet" href="/assets/css/admin.css">
<main style="min-height: 100vh;">

  <?php $manager->loadAdminSidebar($type); ?>
  <div class="position-relative overflow-hidden tab-content text-center bg-light" style="width:100%;padding:1vh;">
    <?php $manager->loadBackendView($type); ?>
  </div>




</main>
<?php 
$manager->loadScript('admin.js');