 <?php 


require_once(BASE_DIR . '/app/Manager.php');

$manager = new listory\Helpers\Manager();
$manager->checkDatabaseStatus();
$manager->validateLoggedIn();

?>



<div class="bg-light" id="admin-settings" style="width:100%">
  <h2>
    Einstellungen
  </h2>
  <hr>

  <div class="admin-tab-content">

    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        
        <button class="nav-link active" id="websitesettings-tab" data-bs-toggle="tab" data-bs-target="#websitesettings" type="button" role="tab" aria-controls="websitesettings" aria-selected="true">Website-Einstellungen</button>
        
        <button class="nav-link" id="usersettings-tab" data-bs-toggle="tab" data-bs-target="#usersettings" type="button" role="tab" aria-controls="usersettings" aria-selected="false">Benutzer-Einstellungen</button> 

        <button class="nav-link" id="faq-tab" data-bs-toggle="tab" data-bs-target="#faq" type="button" role="tab" aria-controls="faq" aria-selected="false">FAQ</button>
      </div>
    </nav>


    <!-- Site information -->
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="websitesettings" role="tabpanel" aria-labelledby="websitesettings-tab">
        <div class="accordion" id="website-settings-accordion">

          <div class="accordion-item">
            <h2 class="accordion-header" id="website-info">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#siteinfo-collapse" aria-expanded="false" aria-controls="siteinfo-collapse">
                Seiteninformationen
              </button>
            </h2>
            <div id="siteinfo-collapse" class="accordion-collapse collapse" aria-labelledby="website-info" data-bs-parent="#website-settings-accordion">
              <div class="accordion-body">
                  <?php foreach ($manager->getOptions('siteinfo') as $option) {
                    ?> 
                    <div class="form-floating mb-3">
                      <input type="text" class="form-control" id="<?= $option['ID']; ?>" value="<?= $option['value']; ?>">
                      <label for="<?= $option['ID']; ?>"><?= $option['label']; ?></label>
                    </div>

                    <?php } ?>

                    <div class="row">
                      <div class="col-9"></div>
                      <div class="col-3 d-grid">
                        <button type="button" class="btn btn-outline-primary">Speichern</button>
                      </div>
                    </div>

              </div>
            </div>
          </div>

          <div class="accordion-item">
            <h2 class="accordion-header" id="websitesettings-header">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#header-collapse" aria-expanded="false" aria-controls="header-collapse">
                Header
              </button>
            </h2>
            <div id="header-collapse" class="accordion-collapse collapse" aria-labelledby="websitesettings-header" data-bs-parent="#website-settings-accordion">
              <div class="accordion-body">
                    <?php foreach ($manager->getOptions('headeroption') as $option) {
                      ?> 
                      <div class="form-floating mb-3">
                        <textarea class="form-control" id="<?= $option['ID']; ?>"><?= $option['value']; ?></textarea>
                        <label for="<?= $option['ID']; ?>"><?= $option['label']; ?></label>
                      </div>

                      <?php } ?>


                      <div class="row">
                        <div class="col-9"></div>
                        <div class="col-3 d-grid">
                          <button type="button" class="btn btn-outline-primary">Speichern</button>
                        </div>
                      </div>

              </div>
            </div>
          </div>

          <div class="accordion-item">
            <h2 class="accordion-header" id="websitesettings-footer">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#footer-collapse" aria-expanded="false" aria-controls="footer-collapse">
                Footer
              </button>
            </h2>
            <div id="footer-collapse" class="accordion-collapse collapse" aria-labelledby="websitesettings-footer" data-bs-parent="#website-settings-accordion">
              <div class="accordion-body">
                
                    <?php foreach ($manager->getOptions('footeroption') as $option) {
                      ?> 
                      <div class="form-floating mb-3">
                        <textarea class="form-control" id="<?= $option['ID']; ?>"><?= $option['value']; ?></textarea>
                        <label for="<?= $option['ID']; ?>"><?= $option['label']; ?></label>
                      </div>

                      <?php } ?>


                      <div class="row">
                        <div class="col-9"></div>
                        <div class="col-3 d-grid">
                          <button type="button" class="btn btn-outline-primary">Speichern</button>
                        </div>
                      </div>
              </div>
            </div>
          </div>

        </div>
      </div>


    <!-- User settings -->
    <div class="tab-pane fade" id="usersettings" role="tabpanel" aria-labelledby="usersettings-tab">...</div>


    <!-- FAQ Tab -->
    <div class="tab-pane fade" id="faq" role="tabpanel" aria-labelledby="faq-tab">
          <div class="accordion-item">
            <h2 class="accordion-header" id="websitesettings-faq-question">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#faq-question-collapse" aria-expanded="false" aria-controls="faq-question-collapse">
                FAQ Fragen
              </button>
            </h2>
            <div id="faq-question-collapse" class="accordion-collapse" aria-labelledby="websitesettings-faq-question" data-bs-parent="#website-settings-accordion">
              <div class="accordion-body">
                <form id="faqs">
      <?php foreach($manager->getFAQs() as $faq) { ?>
                      <div class="mb-3">
                          <input type="hidden" name="questionID" value="<?= $faq['ID']; ?>">
                        <div class="form-floating">
                          <input type="text" name="question-<?= $faq['ID'];?>" class="form-control" id="question-<?= $faq['ID'];?>" value="<?= $faq['question'];?>">
                          <label for="question-<?= $faq['ID'];?>">Q<small>uestion</small></label>
                        </div>


                        <div class="form-floating">
                          <textarea class="form-control" name="answer-<?= $faq['ID'];?>" id="answer-<?= $faq['ID'];?>" style="height: 10rem;"><?= $faq['answer'];?></textarea>
                          <label for="answer-<?= $faq['ID'];?>">A<small>nswer</small></label>
                        </div>
                      </div>
      <?php } ?>
                </form>
                <div class="row">
                  <div class="col-9"></div>
                  <div class="col-3 d-grid">
                    <button type="button" name="saveFAQs" class="btn btn-outline-primary">Speichern</button>
                  </div>
                </div> 

              </div>
            </div>
          </div>
      </div>


    </div>
  </div>

</div>