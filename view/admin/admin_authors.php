<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

?>
<link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css">
<div class="bg-light" id="admin-catalog" style="width:100%">
  <h2>Autoren</h2><hr>
  <div class="admin-tab-content">
    <table id="authortable" class="table table-striped table-hover">
      <thead class="table-primary">
        <tr>
          <th>ID</th>
          <th>Autor</th>
          <th>Buchanzahl</th>
          <th>Aktion</th>
        </tr>
      </thead>
      <tbody>
      <?php 
      foreach ($manager->getAuthors() as $author) {
        ?>

        <tr>
          <td><?= $author['ID']; ?></td>
          <td><?= $author['author']; ?></td>
          <td><?= $author['bookscount']; ?></td>
          <td>
            <a href="/holygrail/panel/authors/edit/<?=  $author['ID']; ?>/">Bearbeiten</a>
          </td>
        </tr>

        <?php
      }
      ?>
      </tbody>
      </tbody>
      <tfoot class="table-primary">
        <tr>
          <th colspan="4"></th>
        </tr>
        
      </tfoot>
    </table>

    <hr>
    <div class="row">
      <div class="col-12 d-grid">
        <a href="/holygrail/panel/authors/new/" name="newauthor" class="btn btn-success fixed-custombtn">Autor:in anlegen</a>
      </div>
    </div> 


  </div>
</div>
<?= $manager->loadScript('datatable.js'); ?>

<script type="text/javascript">
  jQuery('#authortable').dataTable( {
      "language": {
        "url": "/assets/local/de_de.json"
      }
    } );
</script>