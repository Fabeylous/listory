<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

$request = explode('?',$_SERVER['REQUEST_URI'])[0];
$r = explode('/', $request);
$productid = $r[5];

$product = $manager->getProduct(false,$productid)[0]; 

?>
<div class="bg-light" id="admin-products" style="width:100%">
  <h2>Bilder bearbeiten von: <?= $product['title']; ?></h2><hr>
  <div class="input-fields admin-tab-content ">

  <?php  
  $directory  = BASE_DIR. '/media/catalog/products/'.$productid;
  $images = glob($directory . "/*.jpg");
  $imgArr = array();

  ?> 

  <div class="row mb-5 ">
    
    <div class="col-12 ">
      
      <form action="/app/actions.php?action=uploadfile" method="post" enctype="multipart/form-data">
        <div class="mb-2">
          <h3>Coverbild hochladen</h3>
          <input class="form-control form-control-lg" name="coverimage" id="coverimage" type="file" required>
          <input type="hidden" name="productid" value="<?= $productid; ?>">
        </div>

        <div class="p-2">
          <input class="btn btn-primary btn-block" type="submit" value="Hochladen">
        </div>
      </form>
    </div>
  </div>

  <div class="row">
    <h4>Bestehende Bilder</h4>
    <?php
  foreach($images as $image)
  {
    ?>
      <div class="col-2">
        <div
          class="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
          <img src="<?= '/media/catalog/products/'.$productid.'/'.basename($image); ?>" />
        </div>
      </div>
    <?php 
  }
  ?>
  </div>

  </div>
</div>
<style type="text/css">
  img {
    object-fit: cover;
    width:200px;
  }
</style>