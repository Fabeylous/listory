<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

?>
<link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css">
<div class="bg-light" id="admin-products" style="width:100%">
  <h2>Bücherliste</h2><hr>
  <div class="admin-tab-content table-responsive">
    <table id="producttable" class="table table-striped table-hover">
      <thead class="table-primary">
        <tr>
          <th>ID</th>
          <th>ISBN</th>
          <th>Buchtitel</th>
          <th>Autor</th>
          <th>Menge</th>
          <th>Preis</th>
          <th>Aktionen</th>
        </tr>
      </thead>
      <tbody>
    <?php 
    foreach ($manager->getProducts() as $product) {
      ?>

      <tr>
        <td><?= $product['ID']; ?></td>
        <td><?= $product['isbn']; ?></td>
        <td><?= $product['title']; ?></td>
        <td><?= $product['author']; ?></td>
        <td><?= $product['amount']; ?></td>
        <td><?= $product['price']; ?> €</td>
        <td>
          <div class="btn-group ">
              <a href="/holygrail/panel/products/edit/<?=  $product['ID']; ?>/" data-bs-toggle="tooltip" data-bs-placement="top" class="btn btn-outline-secondary" title="Bearbeiten">
                <i class="bi bi-pencil-square"></i>
                <span class="visually-hidden">Bearbeiten</span>
              </a>
              <a href="/holygrail/panel/products/editimages/<?=  $product['ID']; ?>/" data-bs-toggle="tooltip" data-bs-placement="top" type="button" class="btn btn-outline-secondary" title="Bilder bearbeiten">
                <i class="bi bi-images"></i>
                <span class="visually-hidden">Bilder bearbeiten</span>
              </a><!-- 
              <a type="button" class="btn btn-outline-secondary"><i class="bi bi-images"></i>
              <span class="visually-hidden">Button</span>
              </a> -->
            </div>
        </td>
      </tr>

      <?php
    }
    ?>
    </tbody>
      </tbody>
      <tfoot class="table-primary">
        <tr>
          <th colspan="7"></th>
        </tr>
        
      </tfoot>
    </table>


    <hr>
    <div class="row">
      <div class="col-12 d-grid">
        <a href="/holygrail/panel/products/new/" name="newproduct" class="btn btn-success fixed-custombtn">Buch anlegen</a>
      </div>
    </div> 


  </div>
</div>
<?= $manager->loadScript('datatable.js'); ?>

<script type="text/javascript">
  jQuery('#producttable').dataTable( {
      "language": {
        "url": "/assets/local/de_de.json"
      }
    } );
</script>