<?php 
require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();

$user = $manager->getUser($_SESSION['user']);

$request = explode('?',$_SERVER['REQUEST_URI'])[0];
$r = explode('/', $request);
$productid = $r[5];

$product = $manager->getProduct(false,$productid)[0]; 

?>
<div class="bg-light" id="admin-products" style="width:100%">
  <h2>Buch: <?= $product['title']; ?></h2><hr>

  <div class="input-fields admin-tab-content ">

    <div class="input-group mb-3">
      <span class="input-group-text" id="isbn-description">ISBN</span>
      <input name="isbn" type="text" value="<?= $product['isbn']; ?>" class="form-control productinfo-input" placeholder="ISBN" aria-label="ISBN" aria-describedby="isbn-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="title-description">Buchtitel</span>
      <input name="title" type="text" value="<?= $product['title']; ?>" class="form-control productinfo-input" placeholder="Buchtitel" aria-label="Buchtitel" aria-describedby="title-description">
    </div>

    <div class="input-group mb-3">
      <label class="input-group-text" for="author">Autor</label>
      <select name="author" class="form-select productinfo-input" id="author">
        <? foreach ($manager->getAuthors() as $auth) {
          ?>
            <option <?= ($auth['ID'] == $product['author']? 'selected':'') ?> value="<?= $auth['ID']; ?>"><?= $auth['author'] ?></option>
          <?php
        }
        ?>
      </select>
    </div>

<!--     <div class="input-group mb-3">
      <span class="input-group-text" id="slug-description">URL Slug</span>
      <input name="slug" type="text" value="/////////" class="form-control productinfo-input" placeholder="URL Slug" aria-label="URL Slug" aria-describedby="slug-description">
    </div> -->

    <div class="input-group mb-3">
      <span class="input-group-text" id="published-description">Erstveröffentlichung</span>
      <input name="published" type="date" value="<?= $product['published']; ?>" class="form-control productinfo-input" placeholder="Erstveröffentlichung" aria-label="Erstveröffentlichung" aria-describedby="published-description">
    </div> 

    <div class="input-group mb-3">
        <span class="input-group-text" id="subtitle-description">Subtitel</span>
        <input name="subtitle" type="text" value="<?= $product['subtitle']; ?>" class="form-control productinfo-input" placeholder="Subtitel" aria-label="Subtitel" aria-describedby="subtitle-description">
    </div>


    <div class="input-group mb-3">
      <span class="input-group-text">Beschreibung</span>
      <textarea name="description" class="form-control productinfo-input" productinfo-input aria-label="Beschreibung"><?= $product['description']; ?></textarea>
    </div>

    <div class="input-group mb-3">
      <label class="input-group-text" for="bookgenre">Genre</label>
      <select name="bookgenre" class="form-select productinfo-input" id="bookgenre">
        <? foreach ($manager->getCategories() as $cat) {
          ?>
            <option <?= ($cat['genre'] == $product['genre']? 'selected':'') ?> value="<?= $cat['ID']; ?>"><?= $cat['genre'] ?></option>
          <?php
        }
        ?>
      </select>
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="price-description">Preis</span>
      <input name="price" type="number" step="0.01" value="<?= $product['price']; ?>" class="form-control productinfo-input" placeholder="Preis" aria-label="Preis" aria-describedby="price-description">
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="pages-description">Seitenanzahl</span>
      <input  name="pages" type="number"value="<?= $product['pages']; ?>" class="form-control productinfo-input" placeholder="Seitenanzahl" aria-label="Seitenanzahl" aria-describedby="pages-description">
    </div>

    <div class="input-group mb-3">
      <label class="input-group-text" for="booklanguage">Sprache</label>
      <select name="booklanguage" class="form-select productinfo-input" id="booklanguage">
        <? foreach ($manager->getLanguages() as $lang) {
          ?>
            <option <?= ($lang == $product['lang']? 'selected':'') ?> value="<?= $lang; ?>"><?= $lang ?></option>
          <?php
        }
        ?>
      </select>
    </div>


    <div class="input-group mb-3">
      <span class="input-group-text" id="amount-description">Menge</span>
      <input name="amount" type="text" value="<?= $product['amount']; ?>" class="form-control productinfo-input" placeholder="Menge" aria-label="Menge" aria-describedby="amount-description">
    </div>


    <div class="input-group mb-3">
      <span class="input-group-text" id="intention-description">Intention</span>
      <select name="intention" class="form-select productinfo-input" id="intention">
            <option <?= ('sell' == $product['type']? 'selected':'') ?> value="sell">Verkaufen</option>
            <option <?= ('loan' == $product['type']? 'selected':'') ?> value="loan">Leihen</option>
            <option <?= ('all'  == $product['type']? 'selected':'') ?> value="all">Verkaufen oder leihen</option>
      </select>
    </div>

    <div class="input-group mb-3">
      <span class="input-group-text" id="Highlight-description">Highlight</span>
      <select name="highlighted" class="form-select productinfo-input" id="highlighted">
            <option <?= ('0' == $product['highlighted']? 'selected':'') ?> value="0">Nein</option>
            <option <?= ('1' == $product['highlighted']? 'selected':'') ?> value="1">Ja</option>
      </select>
    </div>
    

      <input  name="ID" type="hidden"class="productinfo-input" value="<?= $product['ID']; ?>">
    <div class="row">
      <div class="col-3 d-grid">
        <button type="button" data-bs-toggle="modal" data-bs-target="#deleteProductModal" class="btn btn-outline-danger">Löschen</button>
      </div>
      <div class="col-6"></div>
      <div class="col-3 d-grid">
        <button type="button" name="editProduct" class="btn btn-outline-primary">Speichern</button>
      </div>
    </div> 




  </div>

  </div>
</div>

<div class="modal fade" id="deleteProductModal" tabindex="-1" aria-labelledby="deleteProductModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteProductModalLabel">Sicher?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-footer">
        <button type="button" name="deleteProduct" class="btn btn-danger">Endgültig löschen</button>
      </div>
    </div>
  </div>
</div>
