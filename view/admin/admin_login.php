<?php 
if(isset($_GET['login']) && $_GET['login'] == 'failed') {
    $extraCSS = ' failedVerification ';
} else {
    $extraCSS = '';
}

require_once(BASE_DIR . '/app/Manager.php');
$manager = new listory\Helpers\Manager();
if($manager->validateLoggedIn()) {
    $manager->loadAdminpanel();
    exit();
}

?>
    <link rel="stylesheet" href="/assets/css/login.css">
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <div class="fadeIn first">
                <a href="<?= BASE_URL; ?>" class="logo-link">
                    <img src="/media/logo.jpg" id="icon" alt="User Icon" />
                </a>
            </div>

            <?php 
            if($extraCSS != '') { echo '<h2 class="alert-danger">Anmeldung fehlgeschalgen.</h2>';}
            ?>
            <!-- Login Form -->
            <form action="<?= BASE_URL; ?>/app/validate.php" method="post">
                <input type="text" id="login" class="fadeIn second <?= $extraCSS; ?>" name="login" placeholder="Username">
                <input type="password" id="password" class="fadeIn third <?= $extraCSS; ?>" name="password" placeholder="Passwort">
                <input type="hidden" id="type" name="type" value="admin">
                <input type="submit" class="fadeIn fourth" value="Log In">
            </form>
    
            <!-- Remind Passowrd -->
            <div id="formFooter">
            <a class="underlineHover forgot" href="#">Passwort vergessen?</a>
            </div>

        </div>
    </div>