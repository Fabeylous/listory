 <?php 


require_once(BASE_DIR . '/app/Manager.php');

$manager = new listory\Helpers\Manager();
$manager->checkDatabaseStatus();
$manager->validateLoggedIn();

?>


<link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css">
<div class="bg-light" id="admin-customers"  style="width:100%">
  <h2>Kunden</h2><hr>
  <div class="admin-tab-content table-responsive">
    <table id="authortable" class="table table-striped table-hover">
      <thead class="table-primary">
        <tr>
          <th>#</th>
          <th>Vorname</th>
          <th>Nachname</th>
          <th>Username</th>
          <th>E-Mail</th>
          <th>Adresse</th>
          <th>Bestellungen</th>
          <th>Aktionen</th>
        </tr>
      </thead>
      <tbody>
    <?php 
    foreach ($manager->getUsers(true) as $user) {
      ?>
      <tr>
        <td><?= $user['ID']; ?></td>
        <td><?= $user['firstname']; ?></td>
        <td><?= $user['lastname']; ?></td>
        <td><?= $user['username']; ?></td>
        <td><?= $user['mail']; ?></td>
        <td><?= $user['street'] . '<br>'.$user['zip'].' '.$user['city'].', '.$user['country']  ?></td>
        <td><?= count($manager->getUserOrders($user['ID'])); ?></td>
        <td>
          <div class="btn-group ">


              <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" class="btn btn-outline-secondary" title="Bearbeiten">
                <i class="bi bi-pencil-square"></i>
                <span class="visually-hidden">Benutzer bearbeiten</span>
              </a>

              <a href="#" data-bs-toggle="tooltip" data-bs-placement="top" type="button" class="btn btn-outline-secondary" title="Bestellungen ansehen">
                <i class="bi bi-card-list"></i>
                <span class="visually-hidden">Bestellungen ansehen</span>
              </a>

          </div>

        </td>
      </tr>


      <?php
    }
    ?>
      </tbody>
      <tfoot class="table-primary">
        <tr>
          <th colspan="8"></th>
        </tr>
        
      </tfoot>
    </table>


    
  </div>
</div>
<?= $manager->loadScript('datatable.js'); ?>

<script type="text/javascript">
  jQuery('#authortable').dataTable( {
      "language": {
        "url": "/assets/local/de_de.json"
      }
    } );
</script>