<?php

session_start();
require_once '../Manager.php';
$manager = new Manager();

if(!$manager->validateLoggedIn()) {
  $manager->loadDeniedAccess();
  exit();
}