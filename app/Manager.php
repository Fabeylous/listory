<?php
namespace listory\Helpers;
require_once('Templator.php');
use listory\Helpers\Templator as Templator;
use mysqli;


Class Manager extends Templator {

	protected  $db_host 	= 'db5005159418.hosting-data.io';
	protected  $db_name 	= 'dbs4315713';
	protected  $db_pass 	= 'FabianAlex123!';
	protected  $db_user 	= 'dbu1679936';
	protected  $salt 		= 'hUv_LzOZVKx8';
	public   $baseurl;
	
	public function __construct()
    {
        $this->baseurl = 'http://listory.fabeylous.com';
    }
	
	/**
	 * 
	 * Checks if database is connectable
	 * @since 1.0.0
	 */
	public function checkDatabaseStatus() {	
		// Create connection
		$conn = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}
	}

	/**
	 * 
	 * Returns database connection as object
	 * @since 1.0.0
	 * @return mysqli
	 */
	public function getConnection() {	
		// Create connection
		$conn = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);

		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}

		return $conn;
	}

	/**
	 * 
	 * Runs SQL script
	 * @since 1.0.0
	 */
	public function runSql($query) {
		$conn = $this->getConnection();
		$result = $conn->query($query); 
		return $result;
	}
	
	/**
	 * 
	 * Returns product array
	 * @param String $type (optional), int $limit (optional), bool $highlighted (optional)
	 * @since 1.0.0
	 * @return array
	 */
	public function getProducts($type = false,$limit = false,$highlighted = false) {

		$connection = $this->getConnection();

        $query = 'SELECT p.* , a.author AS authorname, g.genre AS genrename FROM products AS p';
        $query.= ' JOIN authors AS a ON p.author = a.ID ';
        $query.= ' JOIN categories AS g ON p.genre = g.ID ';
        if($type) {
        	$query .= ' WHERE type = "'.$type.'"';
        }
        if($highlighted && !$type) {
        	$query .= ' WHERE highlighted = 1';
        } 
        if($highlighted && $type) {
        	$query .= ' AND highlighted = 1';

        }
        if($limit) {
        	$query .= ' LIMIT '.$limit.' ';
        }


        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;
	}

	/**
	 * 
	 * Returns single product as array by slug or id
	 * @param String $slug (optional), int $id (optional)
	 * @since 1.0.0
	 * @return array
	 */
	public function getProduct($slug = false, $id = false) {
		if($slug && $id) { 
			return false;
		}

		$connection = $this->getConnection();

        $query = 'SELECT p.* , a.author AS authorname, g.genre AS genrename FROM products AS p';
        $query.= ' JOIN authors AS a ON p.author = a.ID ';
        $query.= ' JOIN categories AS g ON p.genre = g.ID ';
        if($slug) {
        	$query .= ' WHERE p.slug = "'.$slug.'"';
        }elseif($id) {
        	$query .= ' WHERE p.ID = "'.$id.'"';
        }


        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;
	}

	/**
	 * 
	 * Returns all genres
	 * @since 1.0.0
	 * @return array
	 */
	public function getGenres() {
		$connection = $this->getConnection();

		$query = 'SELECT categories.*,COUNT(products.ID) as books FROM `categories`
					LEFT JOIN products ON products.genre = categories.ID
					GROUP BY categories.genre;';


        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;
	}


	/**
	 * 
	 * Returns all authors
	 * @param int $id (optional)
	 * @since 1.0.0
	 * @return array
	 */
	public function getAuthors($id = false) {
		$connection = $this->getConnection();

		if($id) {
	        $query = 'SELECT 
	        				a.*,COUNT(p.ID) as bookscount
	        			FROM 
	        				authors AS a
						LEFT JOIN 
							products AS p 
						ON 
							p.author = a.ID
						WHERE a.ID = '.$id.';';
		} else {
	        $query = 'SELECT 
	        				a.*,COUNT(p.ID) as bookscount
	        			FROM 
	        				authors AS a
						LEFT JOIN 
							products AS p 
						ON 
							p.author = a.ID
						GROUP BY a.ID
						ORDER BY a.author ASC';			
		}

        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;
	}
	
	/**
	 * 
	 * Returns languages
	 * @since 1.0.0
	 * @return array
	 */
	public function getLanguages() {
		return array('DE','IT','EN','FR','RU','ES');
	}

	/**
	 * 
	 * Returns all genres
	 * @since 1.0.0
	 * @return array
	 */
	public function getCategories() {
		$connection = $this->getConnection();

        $query = 'SELECT * FROM categories';

        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;
	}
	

	/**
	 * 
	 * Validates login
	 * @param String $username,$password
	 * @since 1.0.0
	 * @return bool
	 */
	public function validateLogin($username,$password) {
        $connection = $this->getConnection();

        $query = 'SELECT id FROM users 
            WHERE username = "'.mysqli_real_escape_string($connection, $username).'"
            AND   password = "'.mysqli_real_escape_string($connection, sha1($password.$this->salt)).'"';

        $result = $connection->query($query);

        return $result->num_rows;

	}

	/**
	 * 
	 * Returns userid based on username
	 * @param String $username 
	 * @since 1.0.0
	 * @return int
	 */
	public function getUserID($username) {
        $connection = $this->getConnection();

        $query = 'SELECT `ID` FROM users 
            WHERE username = "'.mysqli_real_escape_string($connection, $username).'";';

        $result = $connection->query($query);

        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			return $row['ID'];
		}
		return false;

	}


	/**
	 * 
	 * Returns user based on id
	 * @param int $id 
	 * @since 1.0.0
	 * @return int
	 */
	public function getUser($id) {

		$connection = $this->getConnection();

        $query = 'SELECT * FROM users 
            WHERE `ID` = '.$id.';';

        $result = $connection->query($query);

        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			return $row;
		}
		return false;

	}	

	/**
	 * 
	 * Adds new user
	 * @param array $data 
	 * @since 1.0.0
	 * @return int
	 */
	public function addNewUser($data) {

		$connection = $this->getConnection();

        $query = 'INSERT INTO `users` 
        		(`ID`, `username`, `mail`, `admin`, `firstname`, `lastname`, `street`, `zip`, `city`, `country`, `password`, `sessionkey`) 
        VALUES (NULL, 
        		"'.mysqli_real_escape_string($connection, $data['username']).'", 
    			"'.mysqli_real_escape_string($connection, $data['email']).'",
    			0, 
    			"'.mysqli_real_escape_string($connection, $data['name']).'",
    			"'.mysqli_real_escape_string($connection, $data['surname']).'",
    			"'.mysqli_real_escape_string($connection, $data['street']).'",
    			"'.mysqli_real_escape_string($connection, $data['postalcode']).'",
    			"'.mysqli_real_escape_string($connection, $data['city']).'",
    			"Deutschland", 
    			"'.mysqli_real_escape_string($connection, sha1($data['password'].$this->salt)).'", 
    			NULL);';

        return $this->runSql($query);

	}

	/**
	 * 
	 * Returns all users, optionally only customers
	 * @param bool $onlyCustomers (optional)
	 * @since 1.0.0
	 * @return array
	 */	
	public function getUsers($onlyCustomers = false) {

		$connection = $this->getConnection();

        $query = 'SELECT * FROM users';
        $query .= ($onlyCustomers? ' WHERE admin != 1;':';');

        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;

	}

	/**
	 * 
	 * Returns options based on tag
	 * @param String $optiontag 
	 * @since 1.0.0
	 * @return array
	 */
	public function getOptions($optiontag) {

		$connection = $this->getConnection();

        $query = 'SELECT * FROM options WHERE option_group = "'.$optiontag.'"';

        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;

	}
	/**
	 * 
	 * Returns FAQs
	 * @since 1.0.0
	 * @return array
	 */
	public function getFAQs() {

		$connection = $this->getConnection();

        $query = 'SELECT * FROM faq';

        $result = $connection->query($query);
        $return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;

	}

	/**
	 * 
	 * Checks if logged in user is "real"
	 * @param bool $admin 
	 * @since 1.0.0
	 * @return bool
	 */
	public function validateLoggedIn($admin = true) {
		$connection = $this->getConnection();
		if($admin) {
			$query = 'SELECT username,sessionkey FROM users WHERE admin = 1';
		} else {
			$query = 'SELECT username,sessionkey FROM users';
		}
        $result = $connection->query($query);
        
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			if(sha1($row['username']) == $_SESSION['logged_in'] && $row['sessionkey'] == $_SESSION['session']) {
				return true;
			}
		}
		return false;

	}

	public function checkExisting($string, $condition) {
		$connection = $this->getConnection();

		$query = 'SELECT ID FROM users WHERE '.$condition.' = "'.$string.'";';
		
        $result = $connection->query($query);

        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			return $row;
		}
		return false;
	}



	public function generateRandomString($length = 20) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
  	
  	public function loadScript($filename){
    	echo '<script src="/assets/js/'.$filename.'"></script>';
    }
  
  	public function saveNewOrder($user, $products, $totalPrice,$icity,$istreet,$scity, $sstreet) {
    	$connection = $this->getConnection();
      	$sql = 'INSERT INTO `orders`(`user`, `products`, `price`,`invoice_city`,`invoice_street`,`shipping_city`,`shipping_street`) 
  							VALUES ('.$user.',"'.serialize($products).'",'.str_replace(',','.',$totalPrice).',"'.$icity.'","'.$istreet.'","'.$scity.'","'.$sstreet.'")';
      	return $this->runSql($sql);
    }

    public function checkout($data){
    	$userId = $_SESSION['user'];
    	$products = $_SESSION['cart']['products'];
    	$totalPrice = $_SESSION['cart']['totalPrice'];

    	if ($totalPrice > 0) {
    	$this->saveNewOrder($userId, $products, $totalPrice,$data['zip'] . ' ' . $data['city'],$data['address'] . ' ' . $data['address2'],$data['zip'] . ' ' . $data['city'], $data['address'] . ' ' . $data['address2']);
    	unset($_SESSION['cart']['products']);
        unset($_SESSION['cart']['totalPrice']);
        $_SESSION['cart']['total'] = 0;
    	}
    }

    public function getUserOrders($id = false) {
    	$connection = $this->getConnection();
    	if ($id) {
      		$sql = 'SELECT * FROM `orders` WHERE user = '.$id;
    	} else {
      		$sql = 'SELECT * FROM `orders`';
    	}
      	$result = $connection->query($sql);
      	$return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return;
    }

    public function getOrder($id) {
    	$connection = $this->getConnection();
      	
      	$sql = 'SELECT * FROM `orders` WHERE ID = '.$id;
 
      	$result = $connection->query($sql);
      	$return = array();
        if(!$result) return false;
		while ($row = $result->fetch_assoc()) {
			array_push( $return, $row);
		}
		return $return[0];
    }

}