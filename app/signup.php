<?php 
session_start();
require_once './Manager.php';
$manager = new listory\Helpers\Manager();

extract($_POST);


if($manager->checkExisting($username,'username')) {
    header("Location: ".$manager->baseurl."/signup/?error=username"); 
    exit();
} elseif ($manager->checkExisting($email,'mail')) {
    header("Location: ".$manager->baseurl."/signup/?error=mail"); 
    exit();	
} elseif($password !== $passwordconfirmation) {
    header("Location: ".$manager->baseurl."/signup/?error=password"); 
    exit();
}

if($manager->addNewUser($_POST)) {
    header("Location: ".$manager->baseurl."/login/?success=signup"); 
    exit();  
} else {
    header("Location: ".$manager->baseurl."/signup/?error=unknown"); 
    exit();
}
