<?php 

session_start();
require_once './Manager.php';
$manager = new listory\Helpers\Manager();
session_destroy();

header("Location: ".$manager->baseurl."/login/?success=logout"); 
exit();	