<?php 

namespace listory\Helpers;
require_once('Manager.php');
use listory\Helpers\Manager as Manager;


Class Savior extends Manager {



    public function saveFAQs($id,$q,$a) {
        $c = parent::getConnection();

    	$sql = 'UPDATE `faq` SET 
    				`answer` = "'.mysqli_real_escape_string($c,$a).'",
    				`question` = "'.mysqli_real_escape_string($c,$q).'"
    				WHERE 
    				`faq`.`ID` = '.mysqli_real_escape_string($c,$id).';';
                    
		return parent::runSql($sql);
    }

    public function updateProduct($product) {
        $c = parent::getConnection();

        $sql = 'UPDATE `products` SET 
                    `isbn` = "'.mysqli_real_escape_string($c,$product['isbn']).'",
                    `title` = "'.mysqli_real_escape_string($c,$product['title']).'",
                    `author` = "'.mysqli_real_escape_string($c,$product['author']).'",
                    `subtitle` = "'.mysqli_real_escape_string($c,$product['subtitle']).'",
                    `price` = "'.mysqli_real_escape_string($c,$product['price']).'",
                    `pages` = "'.mysqli_real_escape_string($c,$product['pages']).'",
                    `amount` = "'.mysqli_real_escape_string($c,$product['amount']).'",
                    `genre` = "'.mysqli_real_escape_string($c,$product['bookgenre']).'",
                    `lang` = "'.mysqli_real_escape_string($c,$product['booklanguage']).'",
                    `type` = "'.mysqli_real_escape_string($c,$product['intention']).'",
                    `description` = "'.mysqli_real_escape_string($c,$product['description']).'",
                    `highlighted` = "'.mysqli_real_escape_string($c,$product['highlighted']).'"
                    WHERE 
                    `products`.`ID` = '.mysqli_real_escape_string($c,$product['ID']).';';
                    
        return parent::runSql($sql);
    }

    public function addProduct($product) {
        $c = parent::getConnection();  
        $sql = 'INSERT INTO `products`(`ID`, `isbn`, `title`, `genre`, `author`, `price`, `amount`, `subtitle`, `description`, `type`, `slug`, `published`, `pages`, `lang`, `highlighted`)
                        VALUES (
                        NULL,
                        "'.mysqli_real_escape_string($c,$product['isbn']).'",
                        "'.mysqli_real_escape_string($c,$product['title']).'",
                        '.mysqli_real_escape_string($c,$product['bookgenre']).',
                        "'.mysqli_real_escape_string($c,$product['author']).'",
                        '.mysqli_real_escape_string($c,$product['price']).',
                        '.mysqli_real_escape_string($c,$product['amount']).',
                        "'.mysqli_real_escape_string($c,$product['subtitle']).'",
                        "'.mysqli_real_escape_string($c,$product['description']).'",
                        "'.mysqli_real_escape_string($c,$product['type']).'",
                        "'.mysqli_real_escape_string($c,$this->slugify($product['title'])).'",
                        "'.mysqli_real_escape_string($c,$product['published']).'",
                        '.mysqli_real_escape_string($c,$product['pages']).',
                        "'.mysqli_real_escape_string($c,$product['booklanguage']).'",
                        0
                    );';

        return parent::runSql($sql);

    }


    public function removeProduct($product) {
        $c = parent::getConnection();
        require_once('../load.php');
        $path = BASE_DIR.'/media/catalog/products/'.$product['ID'];
        $sql = 'DELETE FROM `products` 
                    WHERE  `ID` = '.$product['ID'].';';
        if(parent::runSql($sql)) {
            array_map('unlink', glob("$path/*.*"));
            rmdir($path);
        } else {
            return false;
        }
        return true;
    }



    public function updateAuthor($authordata) {
        $c = parent::getConnection();

        $sql = 'UPDATE `authors` SET 
                    `author` = "'.mysqli_real_escape_string($c,$authordata['author']).'",
                    `birthday` = "'.mysqli_real_escape_string($c,$authordata['birthday']).'",
                    `nationality` = "'.mysqli_real_escape_string($c,$authordata['nationality']).'"
                    WHERE 
                    `authors`.`ID` = '.mysqli_real_escape_string($c,$authordata['ID']).';';
        return parent::runSql($sql);
    }

    public function newAuthor($authordata) {
        $c = parent::getConnection();

        $sql = 'INSERT INTO `authors` 
                    (
                        `ID` ,
                        `author` ,
                        `birthday`,
                        `nationality`
                    )
                    VALUES (
                        NULL,
                        "'.mysqli_real_escape_string($c,$authordata['author']).'",
                        "'.mysqli_real_escape_string($c,$authordata['birthday']).'",
                        "'.mysqli_real_escape_string($c,$authordata['nationality']).'"
                    );';

        return parent::runSql($sql);
    }

    public function removeAuthor($authordata) {
        $c = parent::getConnection();

        $sql = 'DELETE FROM `authors` 
                    WHERE  `ID` = '.$authordata['ID'].';';

        return parent::runSql($sql);
    }

    public function editProfileinfo($data) {
        session_start();
        $c = parent::getConnection();
        $data['id'] = $_SESSION['user'];
        $username = parent::getUser($data['id'])['username'];

        if(parent::validateLogin($username,$data['currentPassword'])) {

        $sql = 'UPDATE `users` SET 
                    `username` = "'.mysqli_real_escape_string($c,$data['username']).'",
                    `mail` = "'.mysqli_real_escape_string($c,$data['mail']).'",
                    `firstname` = "'.mysqli_real_escape_string($c,$data['firstName']).'",
                    `lastname` = "'.mysqli_real_escape_string($c,$data['lastName']).'",
                    `street` = "'.mysqli_real_escape_string($c,$data['street']).'",
                    `zip` = "'.mysqli_real_escape_string($c,$data['zip']).'",
                    `city` = "'.mysqli_real_escape_string($c,$data['city']).'",
                    `country` = "'.mysqli_real_escape_string($c,$data['country']).'"
                    ';
        if(!empty($data['newPassword']) && !empty($data['passwordConfirmation']) && $data['newPassword'] == $data['passwordConfirmation']) {
            $sql .=    ',`password` = "'.mysqli_real_escape_string($c,sha1($data['newPassword'].$this->salt)).'"';
        }
        $sql .=    'WHERE 
                    `ID` = '.mysqli_real_escape_string($c,$data['id']).';';
                    
        return parent::runSql($sql);

        }
    }


    public function slugify($text, string $divider = '-')
    {

        $text = str_replace(array('ß','ö','Ö','ä','Ä','Ü','ü'), array('ss','oe','Oe','ae','Ae','Ue','ue'), $text);
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
