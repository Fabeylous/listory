<?php 
require_once('Savior.php');
$savior = new listory\Helpers\Savior();




/** 
 * 
 * Page actions
 * 
 */
if($_GET['action'] == 'faq') {
	$questions = $_POST;

	foreach ($questions as $id => $question) {
		if(!$savior->saveFAQs($id,$question['q'],$question['a'])) {
			echo 'false';
			return;
		}
	}
	echo 'true';
	return;
}

/** 
 * 
 * Product actions
 * 
 */
if($_GET['action'] == 'editProduct') {
	$productdata = $_POST;
	if($savior->updateProduct($productdata)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}
if($_GET['action'] == 'addProduct') {
	$productdata = $_POST;
	if($savior->addProduct($productdata)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}
if($_GET['action'] == 'deleteProduct') {
	$productdata = $_POST;
	if($savior->removeProduct($productdata)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}
if($_GET['action'] == 'uploadfile') {

	$imagedata = $_FILES;
	$productid = $_POST['productid'];

	require_once('../load.php');

	if($imagedata['coverimage']['error'] != 0 ) {
		header('Location: /holygrail/panel/products/editimages/'.$productid.'/?upload=failed');
		exit();
	} else {
		$path = BASE_DIR.'/media/catalog/products/'.$productid;
		if(file_exists($path.'/cover.jpg')) {
			$i = 1;
			while(file_exists($path.'/upload_'.$i.'.jpg')) {
				$i++;
			}
			move_uploaded_file($imagedata["coverimage"]["tmp_name"], $path.'/upload_'.$i.'.jpg');
		} else {
			mkdir($path);
			move_uploaded_file($imagedata["coverimage"]["tmp_name"], $path.'/cover.jpg');
		}

		header('Location: /holygrail/panel/products/editimages/'.$productid.'/?upload=success');
		exit();
	}
}


/** 
 * 
 * Author actions
 * 
 */
if($_GET['action'] == 'editAuthor') {
	$authordata = $_POST;

	if($savior->updateAuthor($authordata)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}
if($_GET['action'] == 'newAuthor') {
	$authordata = $_POST;
	
	if($savior->newAuthor($authordata)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}
if($_GET['action'] == 'deleteAuthor') {
	$authordata = $_POST;
	
	if($savior->removeAuthor($authordata)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}


/** 
 * 
 * Profile actions
 * 
 */
if($_GET['action'] == 'editProfileinfo') {
	$data =  $_POST;
	if($savior->editProfileinfo($data)) {
		echo 'true';
	} else {
		echo 'false';
	}
	return;
}
