<?php
require_once('app/Manager.php');

Class JSON  {
	
  	private $manager; 
  	public $key; 
  	public $json; 
  	
  	public function __construct() 
    {
      	$manager = new Manager; 
    	$this->manager = $manager;
    }
  	
	public function createJSON(){
      	$products = $this->manager->getProducts();
       	$json = json_encode($products);
      	return $json;
    }
  
  	public function createAccess() {
		$privKey = sha1($this->manager->generateRandomString(9));
      	return $privKey; 
    }
  
	public function validateKey($key){
      	$validation = false; 
    	$origKey = $this->createAccess();
      	if ($key == $origKey) {
          	return true;
        } else {
          	return false;
        }
    }
}