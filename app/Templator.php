<?php
namespace listory\Helpers;

class Templator {

	public function loadHeader() {
		require_once(BASE_DIR . '/view/header.php');
	}

	public function loadFooter() {
		require_once(BASE_DIR . '/view/footer.php');
	}

	public function loadAdminlogin() {
	    require_once(BASE_DIR . '/view/admin/admin_login.php');
	}

	public function loadAdminpanel($type = 'dashboard') {
	    require_once(BASE_DIR . '/view/admin/admin_panel.php');
	}

	public function loadHome($headhome = false) {
	    require_once(BASE_DIR . '/view/frontend/home.php');
	}	

	public function load404() {
	    require_once(BASE_DIR . '/view/frontend/404.php');
	}

	public function loadDeniedAccess() {
	    require_once(BASE_DIR . '/view/admin/denied.php');
	}	

	public function loadAdminSidebar($type = 'dashboard') {
	    require_once(BASE_DIR . '/view/admin/admin_sidebar.php');
	}

	public function loadBackendView($template = 'settings') {
	    require_once(BASE_DIR . '/view/admin/admin_'.$template.'.php');
	}

	public function loadProductpanel() {
	    require_once(BASE_DIR . '/view/admin/admin_productdetail.php');
	}

	public function loadBodyhead($headhome = false) {
	    require_once(BASE_DIR . '/view/bodyhead.php');
	}
  
  	public function loadBookFinder() {
      	require_once(BASE_DIR . '/view/frontend/bookfinder/bookfinder.php');
    }

  	public function loadPDP($product) {
      	require_once(BASE_DIR . '/view/frontend/product/product.php');
    }

  	public function loadCategorypage() {
      	require_once(BASE_DIR . '/view/frontend/bookfinder/genres.php');
    }  	

    public function loadSignup($e = false,$g = false) {
    	$request = $g;
      	require_once(BASE_DIR . '/view/frontend/customer/signup.php');
    }  	

    public function loadCustomerLogin() {
      	require_once(BASE_DIR . '/view/frontend/customer/login.php');
    }  

    public function loadTAC() {
      	require_once(BASE_DIR . '/view/frontend/agb.php');
    }    

    public function loadPolicy() {
      	require_once(BASE_DIR . '/view/frontend/datapolicy.php');
    }    

    public function loadAbout() {
      	require_once(BASE_DIR . '/view/frontend/about.php');
    }    

    public function loadAuthorpage() {
      	require_once(BASE_DIR . '/view/frontend/bookfinder/authors.php');
    }

    public function loadProfile() {
      	require_once(BASE_DIR . '/view/frontend/customer/profile.php');
    }

    public function loadCart() {
      	require_once(BASE_DIR . '/view/frontend/product/cart.php');
    }    

    public function loadOrderdetail($order) {
      	require_once(BASE_DIR . '/view/frontend/customer/orderdetail.php');
    }    

    public function loadCheckout() {
      	require_once(BASE_DIR . '/view/frontend/product/checkout.php');
    }

    public function generateProductcard($product) {
    	?>
		<div class="col-md-3">
    		<div class="card" style="height: 100%;">
                <div class="img-container" style="height: 300px;text-align: center;">
                    <a href="<?= '/product/'.$product['slug'] ?>">
                    	<img src="/media/catalog/products/<?= $product['ID'] ?>/cover.jpg" class="img-fluid" style="height:100%">
                    </a>
                </div>
                <div class="product-detail-container">

                    <div class="d-flex justify-content-between">
                        <h6 class="mb-0"><?= $product['title']; ?></h6> 
                        <span class="text-danger font-weight-bold"><?= number_format($product['price'],2,',','.'); ?>€</span>

                    </div>
                    <small class="text-muted"><?= $product['authorname']; ?></small>
                    <hr>

                    <div class="mt-3"> 
                    	<button class="btn btn-primary btn-block HOVER mb-2" name="addProductToCart" data-productid="<?= $product['ID']; ?>" role="button" style="background-color: #2c0548;">
							<span></span>
							<text>
				        		<i class="bi bi-cart2 me-2"></i>In den Warenkorb
							</text>
						</button>   	

						<a class="btn btn-outline-primary btn-block HOVER mb-2" href="<?= '/product/'.$product['slug'] ?>" role="button" style="background-color: white;">
							<span></span>
							<text>
				        		Produkt ansehen
							</text>
						</a>

                    </div>
                </div>
            </div>
        </div>

    	<?php
    }
}