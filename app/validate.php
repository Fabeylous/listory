<?php 
session_start();
require_once './Manager.php';

$username   = $_POST["login"];
$password   = $_POST["password"];
$type       = $_POST["type"];


$manager = new listory\Helpers\Manager();


if($manager->validateLogin($username, $password)) {
    $rg = $manager->generateRandomString();
    $id = $manager->getUserID($username);
    $_SESSION['logged_in']  = sha1($username);
    $_SESSION['session']    = $rg;
    $_SESSION['user']       = $id;


    $manager->runSql('UPDATE `users` SET `sessionkey` = "'.$rg.'" WHERE `ID` = '.$id.';');

    if($type == 'admin') {
        header("Location: ".$manager->baseurl."/holygrail/panel/"); 
        exit();
    } else {
        header("Location: ".$manager->baseurl."/profile/"); 
        exit();
    }

} else{

    if($type == 'admin') {
        header("Location: ".$manager->baseurl."/holygrail/?login=failed"); 
        exit();
    } else {
        header("Location: ".$manager->baseurl."/login/?login=failed"); 
        exit();
    }

}





?>
