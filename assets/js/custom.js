jQuery(document).ready(function() {

    function isElementInViewport (el) {
        // Special bonus for those using jQuery
        if (typeof jQuery === "function" && el instanceof jQuery) {
            el = el[0];
        }
        var rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
        );
    }

	jQuery(window).scroll(function() {
         if(isElementInViewport(jQuery('#top').get(0))) {
         	jQuery('body').removeClass('scrolled');
         	jQuery('header').removeClass('scrolled');
         } else {
         	jQuery('header').addClass('scrolled');
         	jQuery('body').addClass('scrolled');
         }
	});

      var menuBtn = jQuery('.anchor-link');
      menuBtn.click(()=>{   
        setTimeout(()=>{
          removeHash();
        }, 5); 
      });

      // removeHash function
      // uses HTML5 history API to manipulate the location bar
      function removeHash(){
        history.replaceState('', document.title, window.location.origin + window.location.pathname + window.location.search);
      }

    var scrollSpy = new bootstrap.ScrollSpy(document.body, {
      target: '#navbar'
    })


    jQuery('#same-address').on('click', function() {
        jQuery('#invoiceaddress-container').slideToggle();
    });

    jQuery(document).on('click','button[name="addProductToCart"]', function() {
        var data = {};
        data['id'] = jQuery(this).data('productid');

        if(jQuery(this).hasClass('HOVER')) {
            goAjax(data,'/api/Product.php?action=addToCart',this);
        } else {
            goAjax(data,'/api/Product.php?action=addToCart',this, 'In den Warenkorb legen');
        }
        jQuery('#lblCartCount').html(parseInt(jQuery('#lblCartCount').html())+1);

    })


    jQuery('button[name="increaseAmount"]').on('click', function() {
        var data = {};
        data['id'] = jQuery(this).data('productid');
        goAjax(data,'/api/Product.php?action=addToCart',this);
        jQuery('#lblCartCount').html(parseInt(jQuery('#lblCartCount').html())+1);

    })    

    jQuery('button[name="decreaseAmount"]').on('click', function() {
        var data = {};
        data['id'] = jQuery(this).data('productid');
        goAjax(data,'/api/Product.php?action=removeFromCart',this);
        jQuery('#lblCartCount').html(parseInt(jQuery('#lblCartCount').html())-1);

    })    

    jQuery('button[name="removeProductFromCart"]').on('click', function() {
        var data = {};
        data['id'] = jQuery(this).data('productid');
        data['amount'] = jQuery("input[name='"+jQuery(this).data('element')+"']").val();
        goAjax(data,'/api/Product.php?action=removeFromCartCompletely',this);
        jQuery('#lblCartCount').html(parseInt(jQuery('#lblCartCount').html())-data['amount']);
        setTimeout(function() { location.reload(); },1000);

    })


    jQuery('button[name="saveProfileButton"]').on('click', function() {
        var data = {};
        data['firstName'] = jQuery('#firstName').val();
        data['mail'] = jQuery('#mail').val();
        data['username'] = jQuery('#username').val();
        data['lastName'] = jQuery('#lastName').val();
        data['street'] = jQuery('#street').val();
        data['zip'] = jQuery('#zip').val();
        data['city'] = jQuery('#city').val();
        data['currentPassword'] = jQuery('#currentPassword').val();
        data['newPassword'] = jQuery('#newPassword').val();
        data['passwordConfirmation'] = jQuery('#passwordConfirmation').val();

        if(data['newPassword'] != '') {
            if(data['newPassword'] != data['passwordConfirmation']) {
                alert('Passwörter stimmen nicht überein.');
                return; 
            }
        }

        if(data['currentPassword'] == '') {
                alert('Bitte aktuelles Passwort eingeben.');
                return; 
        }

        if(data['currentPassword'] == 'froheostern') {
                window.location.href = "https://www.funcage.com/?";
                return; 
        }


        goAjax(data,'/app/actions.php?action=editProfileinfo',this,'Speichern');
        setTimeout(function() { 
            jQuery('#saveProfileButton').slideUp();
            jQuery('.border-right').find('input').each(function() {
                jQuery(this).attr('disabled','true');
            });
        },3000);

    })


    jQuery('button[name="enableEditProfile"]').on('click', function() {
        jQuery('.border-right').find('input').each(function() {
            jQuery(this).removeAttr('disabled');
        });

        jQuery('#saveProfileButton').slideDown();
    })





    function goAjax(passdata,passurl,that,success = '') {
    jQuery('#preloader').fadeIn('fast');
    var options = {
        url: passurl,
        dataType: "json",
        type: "POST",
        data: passdata,
        complete: function(data) {
            jQuery('#preloader').fadeOut();
            if(data.responseText == 'true') {
                if(success != '')  {
                    jQuery(that).addClass('ajaxcompleted').html('<i class="bi bi-check2-circle"></i> Erfolgreich').attr('disabled','true');
                    setTimeout(function() { 
                        jQuery(that).removeClass('ajaxcompleted').html(success).removeAttr('disabled'); 
                    }, 2000);                    
                }
            } else {
                alert('Fehler aufgetreten. Bitte prüfe alle Angaben.');
                if(success != '')  {
                    setTimeout(function() { 
                        jQuery(that).removeClass('ajaxcompleted').html(success).removeAttr('disabled'); 
                    }, 2000);
                }
            }            
        }
    };
    
    jQuery.ajax( options );
}


});