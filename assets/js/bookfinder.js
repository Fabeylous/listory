$( document ).ready(function() {

  	jQuery('#searchfield-input').on('keyup', function() {
      callProductData(jQuery(this).val());
    });

    jQuery('.clickable-filter').on('click', function() {
      if(jQuery(this).data('filter') == "" || jQuery(this).data('filter') == null ) { return; }
      jQuery('.active-filter').removeClass('active-filter');
      jQuery(this).addClass('active-filter');
      callProductData(jQuery('#searchfield-input').val(), jQuery(this).data('filter'));
    });

    function callProductData(searchTerm, filterInput) {
      jQuery('.filterloader').show();
      jQuery.ajax({
          url: "/api/Bookfinder.php",
          dataType: "html",
          type: "POST",
          data: { searchTerm, filterInput },
          success: function (data) {
            jQuery('#resultfield')[0].innerHTML = data;
            jQuery('.filterloader').fadeOut();
          }
        });
    }


    var urlParams = new URLSearchParams(window.location.search); //get all parameters
    var query = urlParams.get('q'); //get search query param

    if(query != '') {
      jQuery('div[data-filter="'+query+'"]').click();
    }

    jQuery('.filterer').on('click', function() {
      var only = jQuery(this).data('only');
      var dont = jQuery(this).closest('.selection').find('.p-lists').find('.dontfilterme');
      jQuery(this).closest('.selection').find('.p-lists').find('.filterme').each(function() {
        var check = jQuery(this).html().charAt(0).toLowerCase();
        if(only == 'all') {
            jQuery(this).parent().attr('style','');
            jQuery(dont).parent().attr('style','display: none !important');
        } else {

          jQuery(dont).parent().attr('style','');

          if(only.indexOf(check) == -1) {
            jQuery(this).parent().attr('style','display: none !important');
          } else {      
            jQuery(this).parent().attr('style','');
          }
        }

      })
    });

    jQuery('button[name="resetFilter"]').on('click', function() {
      callProductData(null,null);
      jQuery('.active-filter').removeClass('active-filter');
      jQuery('#searchfield-input').val('');
    })
});

	
