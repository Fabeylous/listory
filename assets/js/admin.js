jQuery(document).ready(function() {
    jQuery('#preloader').fadeOut();


	jQuery('button[name="saveFAQs"]').on('click', function() {
		var data = {};
		jQuery('input[name="questionID"').each(function() {
			var id = jQuery(this).val();
			data[id] = {};
			data[id]['q'] = jQuery('#question-'+id).val();
			data[id]['a'] = jQuery('#answer-'+id).val();
		})
	  	
		goAjax(data,"/app/actions.php?action=faq",jQuery(this));

	});

	jQuery('button[name="editProduct"]').on('click', function() {
		var data = {};

		jQuery('input[class*="productinfo-input"]').each(function() {
			data[jQuery(this).attr('name')] = jQuery(this).val();
		});
		jQuery('select[class*="productinfo-input"]').each(function() {
			data[jQuery(this).attr('name')] = jQuery(this).val();
		});
		jQuery('textarea[class*="productinfo-input"]').each(function() {
			data[jQuery(this).attr('name')] = jQuery(this).val();
		});

		goAjax(data,"/app/actions.php?action=editProduct",jQuery(this));
	});


	jQuery('button[name="deleteProduct"]').on('click', function() {
		var data = {};
		data['ID'] = jQuery('input[name="ID"]').val();

		goAjax(data,"/app/actions.php?action=deleteProduct",jQuery(this),'/holygrail/panel/products/');
	});

	jQuery('button[name="editAuthor"]').on('click', function() {
		var data = {};
		data['ID'] = jQuery('input[name="authorid"]').val();
		data['author'] = jQuery('input[name="authorname"]').val();
		data['birthday'] = jQuery('input[name="birthday"]').val();
		data['nationality'] = jQuery('input[name="nationality"]').val();

		goAjax(data,"/app/actions.php?action=editAuthor",jQuery(this));
	});

	jQuery('button[name="addAuthor"]').on('click', function() {
		var data = {};
		data['author'] = jQuery('input[name="authorname"]').val();
		data['birthday'] = jQuery('input[name="birthday"]').val();
		data['nationality'] = jQuery('input[name="nationality"]').val();

		goAjax(data,"/app/actions.php?action=newAuthor",jQuery(this),'/holygrail/panel/authors/');
	});	

	jQuery('button[name="deleteAuthor"]').on('click', function() {
		var data = {};
		data['ID'] = jQuery('input[name="authorid"]').val();

		goAjax(data,"/app/actions.php?action=deleteAuthor",jQuery(this),'/holygrail/panel/authors/');
	});

	jQuery('button[name="newProduct"]').on('click', function() {
		var data = {};

		jQuery('input[class*="productinfo-input"]').each(function() {
			data[jQuery(this).attr('name')] = jQuery(this).val();
		});
		jQuery('select[class*="productinfo-input"]').each(function() {
			data[jQuery(this).attr('name')] = jQuery(this).val();
		});
		jQuery('textarea[class*="productinfo-input"]').each(function() {
			data[jQuery(this).attr('name')] = jQuery(this).val();
		});
		
		goAjax(data,"/app/actions.php?action=addProduct",jQuery(this),'/holygrail/panel/products/');
	});






	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
	  return new bootstrap.Tooltip(tooltipTriggerEl)
	});
});


function goAjax(passdata,passurl,that,redirect = false) {
	jQuery('#preloader').fadeIn('fast');
	var options = {
		url: passurl,
		dataType: "json",
		type: "POST",
		data: passdata,
		complete: function(data) {
			console.log(data.responseText);
			jQuery('#preloader').fadeOut();
			if(data.responseText == 'true') {
				jQuery(that).addClass('ajaxcompleted').html('<i class="bi bi-check2-circle"></i> Erfolgreich').attr('disabled','true');
				if(redirect === false) {
					setTimeout(function() { jQuery(that).removeClass('ajaxcompleted').html('Speichern').removeAttr('disabled'); }, 2000);
				} else {
					setTimeout(function() { window.location.href = redirect; }, 1000);
				}
			} else {
				alert('Fehler aufgetreten. Bitte prüfe alle Angaben.');
				setTimeout(function() { jQuery(that).removeClass('ajaxcompleted').html('Speichern').removeAttr('disabled'); }, 2000);
			}
			
		}
	};

	
	jQuery.ajax( options );
}