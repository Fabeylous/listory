<?php 
session_start();

define('BASE_DIR',__DIR__);
define('BASE_URL', 'http://listory.fabeylous.com');


setlocale(LC_ALL, 'de_DE');

require_once(BASE_DIR . '/app/Manager.php');
use listory\Helpers\Manager as Manager;
use listory\Helpers\JSON    as JSON;


$manager = new Manager();
$manager->checkDatabaseStatus();

$request = explode('?',$_SERVER['REQUEST_URI'])[0];





$manager->loadHeader();

if (strpos($request, '/product/') !== false) {
      $r = explode('/', $request);
      $productslug = $r[2];
      $product = $manager->getProduct($productslug);
      if($product) {
        $manager->loadPDP($product);
        $manager->loadFooter();
        exit;
      }
}elseif (strpos($request, '/holygrail/panel/products/edit/') !== false) {
      $r = explode('/', $request);
      $productid = $r[5];
      $product = $manager->getProduct(false,$productid);
      if($product) {
        $manager->loadAdminpanel('editproduct');
        $manager->loadFooter();
        exit;
      }
}elseif (strpos($request, '/holygrail/panel/authors/edit/') !== false) {
      $r = explode('/', $request);
      $authorid = $r[5];
      $author = $manager->getAuthors($authorid);
      if($author) {
        $manager->loadAdminpanel('editauthor');
        $manager->loadFooter();
        exit;
      }
}elseif (strpos($request, '/holygrail/panel/products/editimages/') !== false) {
      $r = explode('/', $request);
      $orderid = $r[3];
      $product = $manager->getProduct(false,$productid);
      if($product) {
        $manager->loadAdminpanel('editimages');
        $manager->loadFooter();
        exit;
      }
}elseif (strpos($request, '/profile/vieworder/') !== false) {
      $r = explode('/', $request);
      $order = $manager->getOrder($r[3]);
      if($order) {
        $manager->loadOrderdetail($order);
        $manager->loadFooter();
        exit;
      }
}

switch ($request) {
  case '/holygrail/':
      $manager->loadAdminlogin();
    break;
  case '/holygrail/panel/':
      $manager->loadAdminpanel('dashboard');
    break; 
  case '/holygrail/panel/orders/':
      $manager->loadAdminpanel('orders');
    break; 
  case '/holygrail/panel/products/':
      $manager->loadAdminpanel('products');
    break; 
  case '/holygrail/panel/products/new/':
      $manager->loadAdminpanel('newproduct');
    break;
  case '/holygrail/panel/authors/new/':
      $manager->loadAdminpanel('newauthor');
    break;
  case '/holygrail/panel/customers/':
      $manager->loadAdminpanel('customers');
    break; 
  case '/holygrail/panel/settings/':
      $manager->loadAdminpanel('settings');
    break;
  case '/holygrail/panel/authors/':
      $manager->loadAdminpanel('authors');
    break;
  case '/profile/':
      $manager->loadProfile();
    break;
  case '/tac/':
      $manager->loadTAC();
    break;
  case '/about/':
      $manager->loadAbout();
    break;
  case '/policy/':
      $manager->loadPolicy();
    break;
  case '/':
      $manager->loadHome(true);
    break;
  case '/bookfinder/':
		$manager->loadBookFinder();
    	break; 
  case '/genres/':
    $manager->loadCategorypage();
      break;
  case '/signup/':
    $e = isset($_GET['froheostern']);
    $get = $_GET;
    $manager->loadSignup($e,$get);
      break;
  case '/login/':
    $manager->loadCustomerlogin();
      break;
  case '/authors/':
    $manager->loadAuthorpage();
      break;
  case '/cart/':
    $manager->loadCart();
      break;
  case '/checkout/':
    $manager->loadCheckout();
      break;
  default:
      $manager->load404();
    break;
}


$manager->loadFooter();
exit();