<?php 
require_once('../app/Manager.php');
$manager = new listory\Helpers\Manager();

$searchTerm = $_POST['searchTerm'];
$filterInput = $_POST['filterInput'];
$filterInput = explode('-', $filterInput); // [0] => author || genre [1] => ID 

$productCollection = $manager->getProducts();

if(empty($filterInput) && empty($searchTerm)) { 
  foreach ($filterCollection as $product) : 
    $manager->generateProductcard($product);
  endforeach;   
  return;
}

$filterCollection = [];
foreach ($productCollection as $product) {
  $dataAvailable = false;

  if (stripos($product['isbn'], $searchTerm) !== false 
    || stripos($product['title'], $searchTerm) !== false 
    || stripos($product['subtitle'], $searchTerm) !== false) {
    $dataAvailable = true;
  }

  if (empty($searchTerm)) {
    $dataAvailable = true;
  }


  if ($filterInput[1] == $product[$filterInput[0]] && $dataAvailable) {
    $filterCollection[] = $product; 
  } 
}


if (empty($filterCollection)) : ?> 
<div class="col">
  <div class="alert alert-secondary" role="alert">
    Es konnten leider keine Bücher mit diesen Kriterien gefunden werden. Schauen Sie <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank">bei unserem Konkurrenten </a> nach.
  </div>
</div>


<?php endif; 

foreach ($filterCollection as $product) : 
  $manager->generateProductcard($product);
endforeach;        

