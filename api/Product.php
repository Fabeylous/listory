<?php 
session_start();
require_once('../app/Manager.php');
$manager = new listory\Helpers\Manager();


if($_GET['action'] == 'addToCart') {
	if(empty($_SESSION['cart']['products'][$_POST['id']])) {
		$_SESSION['cart']['products'][$_POST['id']] = 1;
	} else {
		$_SESSION['cart']['products'][$_POST['id']] = $_SESSION['cart']['products'][$_POST['id']] + 1;
	}

	if(empty($_SESSION['cart']['total'])) {
		$_SESSION['cart']['total'] = 1;
	} else {
		$_SESSION['cart']['total'] = $_SESSION['cart']['total'] + 1;
	}

	echo 'true';
} elseif($_GET['action'] == 'removeFromCart') {
	$_SESSION['cart']['products'][$_POST['id']] = $_SESSION['cart']['products'][$_POST['id']] - 1;
	$_SESSION['cart']['total'] = $_SESSION['cart']['total'] - 1;

	if($_SESSION['cart']['products'][$_POST['id']] <= 0) unset($_SESSION['cart']['products'][$_POST['id']]);
	if($_SESSION['cart']['total'] < 0) $_SESSION['cart']['total'] = 0;
	echo 'true';

} elseif($_GET['action'] == 'removeFromCartCompletely') {
	unset($_SESSION['cart']['products'][$_POST['id']]);

	$_SESSION['cart']['total'] = $_SESSION['cart']['total'] - $_POST['amount'];

	if($_SESSION['cart']['total'] < 0) $_SESSION['cart']['total'] = 0;
	echo 'true';

} elseif($_GET['action'] == 'addToWishlist') {
	if(!$manager->validateLoggedIn()) { return false; }
}